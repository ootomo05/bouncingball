#pragma once
#include "Engine/GameObject.h"
#include <map>

//セルの種類
enum CELL_TYPE
{
	CELL_TYPE_ROAD = 0, //道
	CELL_TYPE_WALL,     //壁
	CELL_TYPE_RED_FLOOR, //赤床
	CELL_TYPE_GREEN_FLOOR, //緑床
	CELL_TYPE_MAX,
};

class Stage : public GameObject
{
	std::map<CELL_TYPE, float> cost_;  //コストを表す

	int hCellModels_[CELL_TYPE_MAX]; //角マスに配置するモデル番号格納

	unsigned int mapWidht;   //マップ幅
	unsigned int mapHeiht; //マップ高さ

	const float CELL_SIZE = 1.0f; //マスの一辺の長さ
	
	int **ppMapData_; //マップデータ
public:

	//コンストラクタ
	//引数 pParent 親
	Stage(GameObject* pParent);

	//デストラクタ
	~Stage();

	//初期化
	//引数 なし
	virtual void Initialize() override;

	//更新
	//引数 なし
	virtual void Update() override;

	//描画
	//引数 なし
	virtual void Draw() override;

	//更新
	//引数 なし
	virtual void Release() override;

	//マップデータを取得
	//引数 x
	//引数 y
	//戻値
	int GetMapData(const unsigned int x, const unsigned int y);

	//ブロックの1辺の長さを取得
	//引数 なし
	//戻値 1辺の長さ
	float GetBlockSize() { return CELL_SIZE; };

	//幅を取得
	//引数 なし
	//戻値 幅
	unsigned int GetMapWidht() { return mapWidht; }

	//マップの高さを取得
	//引数 なし
	//戻値 高さ
	unsigned int GetMapHeight() { return mapHeiht; }

	//壁かどうか判定
	//引数 x座標
	//引数 y座標 3Dゲームではz座標
	//戻値 壁ならtureを返す
	bool IsWall(unsigned int x, unsigned int y) { return ppMapData_[x][y] == CELL_TYPE_WALL; }

	//マップのデータを更新
	//引数 x
	//引数 y
	//引数 type
	void SetMapData(unsigned int x, unsigned int y, int type) { ppMapData_[x][y] = type; };

	//床の色を更新
	//引数 x
	//引数 y
	//引数 color
	void SetColorFloor(unsigned int x, unsigned int z, int color) { ppMapData_[x][z] = color; };

	//コストを取得
	int GetCost(CELL_TYPE type);
};

