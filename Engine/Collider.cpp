#include "Collider.h"
#include "SphereCollider.h"
#include "BoxCollider.h"
#include "GameObject.h"

//コンストラクタ
Collider::Collider() : 
	pGameObject_(nullptr)
{
}

//デストラクタ
Collider::~Collider()
{
}

//箱同士の当たり判定
bool Collider::IsHitBoxVsBox(BoxCollider * boxA, BoxCollider * boxB)
{
	XMVECTOR boxPosA = boxA->pGameObject_->GetWorldPosition() + boxA->center_; //boxAのモデルから見た位置
	XMVECTOR boxPosB = boxB->pGameObject_->GetWorldPosition() + boxB->center_; //boxBのモデルから見た位置

	//衝突しているか調べる
	if ((boxPosA.vecX + boxA->size_.vecX / 2) > (boxPosB.vecX - boxB->size_.vecX / 2) &&
		(boxPosA.vecX - boxA->size_.vecX / 2) < (boxPosB.vecX + boxB->size_.vecX / 2) &&
		(boxPosA.vecY + boxA->size_.vecY / 2) > (boxPosB.vecY - boxB->size_.vecY / 2) &&
		(boxPosA.vecY - boxA->size_.vecY / 2) < (boxPosB.vecY + boxB->size_.vecY / 2) &&
		(boxPosA.vecZ + boxA->size_.vecZ / 2) > (boxPosB.vecZ - boxB->size_.vecZ / 2) &&
		(boxPosA.vecZ - boxA->size_.vecZ / 2) < (boxPosB.vecZ + boxB->size_.vecZ / 2))
	{
		return true; //衝突している
	}

	return false; //衝突していない
}

//箱と球体の衝突判定
bool Collider::IsHitBoxVsCircle(BoxCollider * box, SphereCollider * sphere)
{
	//それぞれのコライダーの位置(オブジェクトから見た)
	XMVECTOR circlePos = sphere->pGameObject_->GetWorldPosition() + sphere->center_;
	XMVECTOR boxPos = box->pGameObject_->GetWorldPosition() + box->center_;

	if (circlePos.vecX > boxPos.vecX - box->size_.vecX - sphere->size_.vecX &&
		circlePos.vecX < boxPos.vecX + box->size_.vecX + sphere->size_.vecX &&
		circlePos.vecY > boxPos.vecY - box->size_.vecY - sphere->size_.vecY &&
		circlePos.vecY < boxPos.vecY + box->size_.vecY + sphere->size_.vecY &&
		circlePos.vecZ > boxPos.vecZ - box->size_.vecZ - sphere->size_.vecZ &&
		circlePos.vecZ < boxPos.vecZ + box->size_.vecZ + sphere->size_.vecZ)
	{
		return true; //衝突している
	}

	return false; //衝突していない
}

//球体同士の衝突判定
bool Collider::IsHitCircleVsCircle(SphereCollider * circleA, SphereCollider * circleB)
{
	XMVECTOR vec = (circleA->center_ + circleA->pGameObject_->GetWorldPosition()) - (circleB->center_ + circleB->pGameObject_->GetWorldPosition());//球体同士の距離
	vec = XMVector3Length(vec);

	//衝突しているか調べる
	if (vec.vecX <= (circleA->size_.vecX + circleB->size_.vecX))
	{
		return true; //衝突している
	}

	return false; //衝突していない
}

//半径を返す
float Collider::GetRadius()
{
	return radius_; //半径
}

//ゲームオブジェクトのセット
void Collider::SetGameObject(GameObject * gameObj)
{
	pGameObject_ = gameObj;
}
