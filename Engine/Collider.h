#pragma once
#include <DirectXMath.h>

using namespace DirectX;

//プロトタイプ宣言
class GameObject;
class BoxCollider;
class SphereCollider;

//コライダーの種類
enum ClolliderType
{
	COLLIDER_BOX = 0, //箱型コライダー
	COLLIDER_CIRCLE   //球体コライダー
};

//当たり判定用の情報
class Collider
{
	friend class BoxCollider;
	friend class SphereCollider;
protected:
	GameObject* pGameObject_; //このコライダーを付けたオブジェクトを参照する
	ClolliderType type_; //コライダーの種類
	XMVECTOR center_; //オブジェクトから見た球の中心
	XMVECTOR size_;   //コライダーのサイズ
	float radius_; //半径
public:

	//コンストラクタ
	Collider();

	//デストラクタ
	virtual ~Collider();

	//当たっているか調べる
	//引数 pTarget 相手のコライダー
	//戻り値 当たっていればtrueを返す
	virtual bool IsHit(Collider* target) = 0;

	//箱同士の衝突判定
	//引数 boxA
	//引数 boxA
	//戻値 衝突していたらtrueを返す
	bool IsHitBoxVsBox(BoxCollider* boxA, BoxCollider* boxB);

	//箱と球体の衝突判定
	//引数 box
	//引数 spher
	//戻値 衝突していたらtrueを返す
	bool IsHitBoxVsCircle(BoxCollider* box, SphereCollider* spher);

	//球体同士の衝突判定
	//引数 circleA
	//引数 circleB
	//戻値 衝突していたらtrueを返す
	bool IsHitCircleVsCircle(SphereCollider* circleA, SphereCollider* circleB);
	//半径を返す
	//引数 なし
	//戻り値 なし
	float GetRadius();

	//このコライダーを付けたオブジェクトのアドレスをセットする
	//引数 gameObj このコライダーを付けたオブジェクトのアドレス
	void SetGameObject(GameObject* gameObj);
};

