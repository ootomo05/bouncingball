#include "Quad.h"
#include "Camera.h"
#include "Transform.h"

//コンストラクタ
Quad::Quad() : 
	pVertexBuffer_(nullptr), pIndexBuffer_(nullptr), pConstantBuffer_(nullptr),
	indexElementCount_(0), pTexture_(nullptr)
{
}

//デストラクタ
Quad::~Quad()
{
}

//初期化
HRESULT Quad::Initialize(std::string imgFileName)
{
	//頂点バッファ作成
	if (FAILED(InitVertexBuffer()))
	{
		return E_FAIL;
	}

	//インデックスバッファ作成
	if (FAILED(InitIndexBuffer()))
	{
		return E_FAIL;
	}
	
	//コンスタントバッファ作成
	if (FAILED(InitConstantBuffer()))
	{
		return E_FAIL;
	}
	
	//画像データを読み込む
	//読み込めたかチェック
	pTexture_ = new Texture;
	if (FAILED(pTexture_->Load(imgFileName)))
	{
		return E_FAIL;
	};

	return S_OK;//何も問題なし
};

//頂点バッファ作成
HRESULT Quad::InitVertexBuffer()
{
	//頂点情報
	//図形をいじるときに変更するとこ
	//頂点座標の設定
	//vertices[]	それぞれの要素は頂点座標、テクスチャの切り取る座標
	//index[]		三角形を作る時の順番指定
	VERTEX vertices[] =
	{
		{XMVectorSet(-1.0f, 1.0f, 0.0f, 0.0f), XMVectorSet(0.0f, 0.5f, 0.0f, 0.0f)},	// 五角形の頂点（左上） 0
		{XMVectorSet(1.0f, 1.0f, 0.0f, 0.0f), XMVectorSet(1.0f, 0.5f, 0.0f, 0.0f)},	// 五角形の頂点（右上） 1
		{XMVectorSet(1.0f,-1.0f, 0.0f, 0.0f), XMVectorSet(1.0f, 1.0f, 0.0f, 0.0f)},	// 五角形の頂点（右下） 2
		{XMVectorSet(-1.0f,-1.0f, 0.0f, 0.0f), XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f)},	// 五角形の頂点（左下） 3
		{XMVectorSet(0.0f, 2.0f, 0.0f, 0.0f), XMVectorSet(0.5f, 0.0f, 0.0f, 0.0f)},	// 五角形の頂点(上)	 4
	};


	// 頂点データ用バッファの設定
	// 一つ一つの頂点が持つ情報をシェーダに渡せるように
	// pVertexBuffer_に設定してやる
	// 最後にpVertexBuffer_がちゃんと作成できたかチェック
	D3D11_BUFFER_DESC bd_vertex;//頂点データ用バッファ用の構造体
	bd_vertex.ByteWidth = sizeof(vertices);
	bd_vertex.Usage = D3D11_USAGE_DEFAULT;
	bd_vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd_vertex.CPUAccessFlags = 0;
	bd_vertex.MiscFlags = 0;
	bd_vertex.StructureByteStride = 0;
	D3D11_SUBRESOURCE_DATA data_vertex;
	data_vertex.pSysMem = vertices;//入れたい情報
	if (FAILED(Direct3D::pDevice->CreateBuffer(&bd_vertex, &data_vertex, &pVertexBuffer_)))
	{
		MessageBox(nullptr, "Quad.cpp：頂点バッファ作成失敗", "エラー", MB_OK);
		return E_FAIL;          //:pVertexBuffer_作成でなんか問題あるぞ！
	}

	return S_OK;
};

//インデックスバッファ作成
HRESULT Quad::InitIndexBuffer()
{
	//インデックス情報
	//作る三角形頂点を結ぶ順番を作る三角形分入れる3つで一組
	//時計回りで指定してやる
	//頂点を増やしたときはここもいじる
	int index[] = { 0,2,3, 0,1,2, 0,4,1 };
	indexElementCount_ = sizeof(index) / sizeof(int);//DrawIndexedの第一引数用

	// インデックスバッファを生成する
	D3D11_BUFFER_DESC   bd;//インデックスバッファ用の構造体
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(index);
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = index;
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;
	if (FAILED(Direct3D::pDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer_)))
	{
		MessageBox(nullptr, "Quad.cpp：インデックスバッファ作成失敗", "エラー", MB_OK);
		return E_FAIL;//:pIndexBuffer_作成でなんか問題あるぞ！
	}

	return S_OK;
};

//コンスタントバッファ作成
HRESULT Quad::InitConstantBuffer()
{
	//コンスタントバッファ作成
	//モデルに付随するカメラやライトの情報を渡す
	D3D11_BUFFER_DESC cb;//コンスタントバッファ用の構造体
	cb.ByteWidth = sizeof(CONSTANT_BUFFER);
	cb.Usage = D3D11_USAGE_DYNAMIC;
	cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cb.MiscFlags = 0;
	cb.StructureByteStride = 0;
	//pConstantBuffer生成
	if (FAILED(Direct3D::pDevice->CreateBuffer(&cb, nullptr, &pConstantBuffer_)))
	{
		MessageBox(nullptr, "Quad.cpp：コンスタントバッファ作成失敗", "エラー", MB_OK);
		return E_FAIL;//:pConstantBuffer作成でなんか問題あるぞ！
	}


	return S_OK;
};

//描画
void Quad::Draw(Transform& transform)
{
	//使うシェーダの種類を決める
	Direct3D::SetShaderBundle(SHADER_3D);


	//コンスタントバッファに渡す情報
	//今のところカメラ情報のみ渡す
	//XMMatrixTranspose()シェーダ用行列に変換してくれる関数
	CONSTANT_BUFFER cb;
	cb.matWVP = XMMatrixTranspose((transform.GetWorldMatrix() * Camera::GetViewMatrix() *
		Camera::GetProjectionMatrix()));                                  //ワールド行列、ビュー行列、プロジェクション行列を合成したものを渡す
	cb.matW = XMMatrixTranspose(transform.matRotate_ * XMMatrixInverse(nullptr, transform.matScale_));//回転行列を陰の向きを計算するために渡す
	D3D11_MAPPED_SUBRESOURCE pdata;
	Direct3D::pContext->Map(pConstantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata);	// GPUからのデータアクセスを止める
	memcpy_s(pdata.pData, pdata.RowPitch, (void*)(&cb), sizeof(cb));	                // データを値を送る
	
	//テクスチャとサンプラーをシェーダに渡す処理
	ID3D11SamplerState* pSampler = pTexture_->GetSampler();
	Direct3D::pContext->PSSetSamplers(0, 1, &pSampler);

	ID3D11ShaderResourceView* pSRV = pTexture_->GetSRV();
	Direct3D::pContext->PSSetShaderResources(0, 1, &pSRV);

	Direct3D::pContext->Unmap(pConstantBuffer_, 0);	//再開

	//頂点バッファ
	UINT stride = sizeof(VERTEX);
	UINT offset = 0;
	Direct3D::pContext->IASetVertexBuffers(0, 1, &pVertexBuffer_, &stride, &offset);

	// インデックスバッファーをセット
	stride = sizeof(int);
	offset = 0;
	Direct3D::pContext->IASetIndexBuffer(pIndexBuffer_, DXGI_FORMAT_R32_UINT, 0);

	//コンスタントバッファ
	Direct3D::pContext->VSSetConstantBuffers(0, 1, &pConstantBuffer_);	//頂点シェーダー用	
	Direct3D::pContext->PSSetConstantBuffers(0, 1, &pConstantBuffer_);	//ピクセルシェーダー用

	Direct3D::pContext->DrawIndexed(indexElementCount_, 0, 0);//描画
}

//開放
void Quad::Release()
{
	SAFE_RELEASE(pTexture_);
	SAFE_RELEASE(pConstantBuffer_);
	SAFE_RELEASE(pIndexBuffer_);
	SAFE_RELEASE(pVertexBuffer_);
}