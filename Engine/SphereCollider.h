#pragma once
#include <vector>
#include <DirectXMath.h>
#include <d3d11.h>
#include "Collider.h"

//プロトタイプ宣言
class GameObject;

//球体コライダーの情報を管理
class SphereCollider : public Collider
{
	friend class Collider;
public:

	//コンストラクタ
	//引数 center コライダーをつける位置
	//引数 radius 球体の半径
	SphereCollider(DirectX::XMVECTOR center, float radius);

	//デストラクタ
	~SphereCollider();

	//衝突判定
	//引数 target 衝突しているか調べたい相手
	//戻値 衝突していればtrueを返す
	bool IsHit(Collider * target) override;
};