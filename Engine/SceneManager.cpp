#include "SceneManager.h"

#include "../Scene/ResultScene.h"
#include "../Scene/TitleScene.h"
#include "../Scene/GameScene.h"
#include "Model.h"
#include "Image.h"

SceneManager::SceneManager(GameObject * parent)
	: GameObject(parent, "SceneManager"),nextSceneID_(SCENE_ID_TITLE)
{
}

//初期化
void SceneManager::Initialize()
{
	//最初に呼ばれるシーン
	Instantiate<TitleScene>(this);
	nowSceneID_ = SCENE_ID_TITLE;
}

//更新
void SceneManager::Update()
{
	//シーンを切り替える処理
	if (nowSceneID_ != nextSceneID_)
	{
		//新しいシーンにするために
		//片づける処理
		DeleteAllChildren(); //現在の子供以下をすべて削除

		Model::AllDelete(); //現在のシーンで使っている3Dモデルを開放
		Image::Relese();    //現在のシーンで使っている画像を開放
		
		//次のシーンを作成する
		switch (nextSceneID_)
		{
			case SCENE_ID_GAME:Instantiate<GameScene>(this); break;  //テストシーン
			case SCENE_ID_TITLE:Instantiate<TitleScene>(this); break;//タイトルシーン
			case SCENE_ID_RESULT:Instantiate<ResultScene>(this); break; //リザルトシーン
		}

		nowSceneID_ = nextSceneID_; //現在のシーンを更新
	}
}

//描画
void SceneManager::Draw()
{
}

//開放
void SceneManager::Release()
{
}

//シーンを切り替える
void SceneManager::ChangeScene(SCENE_ID next)
{
	nextSceneID_ = next;
}
