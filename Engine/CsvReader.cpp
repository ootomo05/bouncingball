#include <Windows.h>
#include "CsvReader.h"
#include "Global.h"

CsvReader::CsvReader()
{
	data_.clear(); //初期化
}

CsvReader::~CsvReader()
{
	//配列の解放
	for (UINT y = 0; y < data_.size(); y++)
	{
		for (UINT x = 0; x < data_[y].size(); x++)
		{
			data_[y][x].clear();
		}
	}
}

bool CsvReader::Load(std::string fileName)
{
	//ファイル読み込み
	HANDLE hFile;
	hFile = CreateFile(fileName.c_str(), GENERIC_READ, 0, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

	//読み込めたかチェック
	if (hFile == INVALID_HANDLE_VALUE)
	{
		std::string message = fileName + "を読み込めませんでした";
		MessageBox(nullptr, message.c_str(), "CsvReader ERROR", MB_OK);

		return false;
	}

	//ファイルサイズを取得
	DWORD fileSize = GetFileSize(hFile, nullptr);

	//ファイルからデータを読み込む
	char* temp;
	temp = new char[fileSize];

	
	DWORD dwBytes = 0; //何バイト目を参照しているか記憶
	ReadFile(hFile, temp, fileSize, &dwBytes, nullptr); //ファイル読み込む

	CloseHandle(hFile); //閉じる

	std::vector<std::string> line; //

	DWORD index = 0;

	while (index < fileSize)
	{
		std::string val = "";
		GetToComma(&val, temp, &index);

		if (val.length() - 1 == 0)
		{
			data_.push_back(line);

			line.clear();

			continue;
		}

		line.push_back(val);
	}

	SAFE_DELETE_ARRAY(temp);

	return true;
}

void CsvReader::GetToComma(std::string * result, std::string data, unsigned long * index)
{
	while (data[*index] != ',' && data[*index] != '\n' && data[*index] != '\r')
	{
		*result += data[*index];
		(*index)++;
	}

	*result += '\0';
	(*index)++;
}

int CsvReader::GetValue(unsigned int x, unsigned int y)
{
	return atoi(GetString(x, y).c_str());
}

std::string CsvReader::GetString(const unsigned int x, const unsigned int y)
{
	if (x < 0 || x >= GetWidth() || y < 0 || y >= GetHeight())
	{
		return "";
	}
	return data_[y][x];
}

size_t CsvReader::GetWidth()
{
	return data_[0].size();
}

size_t CsvReader::GetHeight()
{
	return data_.size();
}
