#include "GameObject.h"
#include "Collider.h"
#include "SphereCollider.h"

//引数なしのコンストラクタが呼ばれたとき
//引数ありのほうにnullと""を渡す
GameObject::GameObject() : GameObject(nullptr,"")
{
}

//コンストラクタ
GameObject::GameObject(GameObject * parent, const std::string & name) :
	pParent_(parent),objectName_(name),colliders_(nullptr)
{
}

//デストラクタ
GameObject::~GameObject()
{
	DeleteAllChildren(); //子供をすべて削除
}

//すべての子供の描画を行う
void GameObject::DrawSub()
{
	//自分の描画
	Draw();
	//子供すべてを描画する
	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		(*itr)->DrawSub();		
	}
}

//すべての子供の更新を行う
void GameObject::UpdateSub()
{
	//親の座標を決めてから
	Update();

	//座標の計算
	transform_.Calclation();

	//子供の更新処理をよぶ
	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		(*itr)->UpdateSub();
	}

	//削除する子供がいないか調べる
	for (auto itr = childList_.begin(); itr != childList_.end();)
	{
		//死亡フラグが立っている子供を消す
		if ((*itr)->IsDead() == true)
		{		
			//削除処理
			(*itr)->ReleaseSub();
			SAFE_DELETE(*itr);
			//itrに次のイテレータを代入しないと
			//次に行く場所を見失うので代入
			itr = childList_.erase(itr);
		}
		else
		{
			//当たり判定を行う
			(*itr)->Collision(GetRootJob());

			//ここでitr++してやらないと
			//変更した現在のitrを飛ばしてしまうので
			//削除したときは次の要素にいかないようにする
			itr++; 
		}
	}
}

//すべての子供の開放を行う
void GameObject::ReleaseSub()
{
	//自分自身の開放を行ってから
	//子供の開放処理を呼ぶ
	Release();

	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		(*itr)->ReleaseSub();
	}

	
}

//座標をセットする
void GameObject::SetPosition(DirectX::XMVECTOR & position){transform_.position_ = position;}

//大きさを変える
void GameObject::SetScale(DirectX::XMVECTOR & scale){transform_.scale_ = scale;}

//回転させる
void GameObject::SetRotate(DirectX::XMVECTOR & rotate){transform_.rotate_ = rotate;}

//座標を取得
DirectX::XMVECTOR GameObject::GetPosition(){return transform_.position_;}

//大きさを取得
DirectX::XMVECTOR GameObject::GetScale(){return transform_.scale_;}

//回転角度を取得
DirectX::XMVECTOR GameObject::GetRotate(){return transform_.rotate_;}

//ワールド座標を取得
DirectX::XMVECTOR GameObject::GetWorldPosition() { return GetParent()->transform_.position_ + transform_.position_; };

//ワールド回転を取得
DirectX::XMVECTOR GameObject::GetWorldRotate() { return GetParent()->transform_.rotate_ + transform_.rotate_; };

//ワールドスケールを取得
DirectX::XMVECTOR GameObject::GetWorldScale() { return GetParent()->transform_.scale_ + transform_.scale_; };

//自分を殺す
void GameObject::KillMe(){isDead_ = true;}

//削除されるかどうか
bool GameObject::IsDead(){return isDead_;}

//子供とコライダーをすべて削除
void GameObject::DeleteAllChildren()
{

	//ループを回す準備
	//子を全部開放
	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		//オブジェクト内の開放処理を行う
		(*itr)->Release();

		//作ったオブジェクト自体の削除
		SAFE_DELETE(*itr);
	}

	childList_.clear(); //リストを開放
	SAFE_DELETE(colliders_); //コライダー削除
}

//子供の中からオブジェクトを探す
GameObject * GameObject::FindChildObject(const std::string name)
{
	//自分の子供から探す
	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{

		//探しているオブジェクトの名前が一致
		if ((*itr)->GetObjectName() == name)
		{
			return *itr; //アクセス先を返す
		}

		//自分から見て孫の中から探す
		GameObject* pObjResult = (*itr)->FindChildObject(name);

		
		//見つかれば何か入る
		if (pObjResult != nullptr)
		{
			return pObjResult; //見つけた時のアドレス
		}		
	}

	return nullptr; //いなかった
}


//rootjobオブジェクト探す
GameObject * GameObject::GetRootJob()
{
	//自分がrootjobなら
	if (pParent_ == nullptr)
	{
		return this;
	}
	else
	{
		//自分がrootjobでないなら必ず自分のご先祖の中にrootjobがいる
		return pParent_->GetRootJob();
	}
	
}

//特定のオブジェクトを探す
GameObject * GameObject::FindObject(const std::string name)
{
	return GetRootJob()->FindChildObject(name);
}

GameObject * GameObject::GetParent()
{
	return pParent_;
}

////~~~~~~~~当たり判定~~~~~~~~~

//総当たりで当たっているか調べる
void GameObject::Collision(GameObject* pTarget)
{

	//どうしようもないので終わる
 	if (pTarget == nullptr)
	{
		return;
	}

	//親と衝突してるか調べる
	if (this->colliders_ != nullptr && pTarget->colliders_ != nullptr && (pTarget != this))
	{

		if (this->colliders_->IsHit(pTarget->colliders_))
		{
			OnCollision(pTarget);
		}
	}

	//子供を調べる
	for (auto itr = pTarget->childList_.begin();itr != pTarget->childList_.end();itr++)
	{
		Collision((*itr));
	}

}

//コライダーを追加
void GameObject::AddCollider(Collider* collider)
{
	collider->SetGameObject(this); //自分の情報を与える
	colliders_ = collider;
}

//コライダー同士が当たっていたら用れる
void GameObject::OnCollision(GameObject * pTarget)
{
}

void GameObject::PushBackChild(GameObject * obj)
{
	assert(obj != nullptr);

	obj->pParent_ = this;
	childList_.push_back(obj);
}