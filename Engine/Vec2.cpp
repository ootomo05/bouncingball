#include "Vec2.h"
#include <iostream>

Vec2 Vec2::Dot(const Vec2 & a)
{
	float dot = this->x * a.x + this->y * a.y;
	return Vec2(dot, dot);
}

Vec2 Vec2::Cross(const Vec2 & a)
{
	float cross = this->x * a.y - a.x * this->y;
	return Vec2(cross, cross);
}
Vec2 Vec2::Length()
{
	float lenght = sqrt(pow(this->x, 2) + pow(this->y, 2));
	return Vec2(lenght, lenght);
}
Vec2 Vec2::Normalize()
{
	float length = Length().x;
	return Vec2(x / length, y / length);
};
