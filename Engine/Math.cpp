#include "Math.h"

//行列式
float Math::Det(XMVECTOR a, XMVECTOR b, XMVECTOR c)
{
	return	(a.vecX * b.vecY * c.vecZ) +
		(a.vecY * b.vecZ * c.vecX) +
		(a.vecZ * b.vecX * c.vecY) -
		(a.vecX * b.vecZ * c.vecY) -
		(a.vecY * b.vecX * c.vecZ) -
		(a.vecZ * b.vecY * c.vecX);
}

//三角形と線分の交差判定
bool Math::Intersect(XMVECTOR origin, XMVECTOR ray, XMVECTOR v0, XMVECTOR v1, XMVECTOR v2,float* t)
{

	//三角形の辺を求める
	XMVECTOR edge1 = v1 - v0; //頂点v0からv1までの辺 a
	XMVECTOR edge2 = v2 - v0; //頂点v0からv2までの辺 b


	//方向なので必ず1になるようにする
	ray = XMVector3Normalize(ray);

	//クラメルの公式の分母用
	//分母だけは変わらない
	float demo = Det(edge1, edge2, -ray);

	//分母が0より小さい
	//レイと三角形は平行なので当たらない
	//計算するのは無駄なのでfalseを返す
	if (demo < 0)
	{
		return false;
	}

	//クラメールの公式から
	float u = Det(origin - v0, edge2, -ray) / demo; //edge1の割合を求める
	float v = Det(edge1, origin - v0,-ray) / demo;  //edge2の割合を求める
	*t = Det(edge1, edge2 - v0, origin - v0) / demo; //レイの発射地点から三角形までの距離

	//レイと三角形の交差判定
	if ((u >= 0 && v >= 0)&&
		(u + v <= 1)&&
		t >= 0)
	{
		return true;
	}

	return false;
};