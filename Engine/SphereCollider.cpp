#include "SphereCollider.h"
#include "GameObject.h"
#include "BoxCollider.h"

//コンストラクタ
SphereCollider::SphereCollider(DirectX::XMVECTOR center, float radius)
{
	center_ = center;
	size_ = XMVectorSet(radius, radius, radius, 0);
	type_ = COLLIDER_CIRCLE;
}

//デストラクタ
SphereCollider::~SphereCollider()
{
}

//衝突判定
bool SphereCollider::IsHit(Collider * target)
{
	//コライダーの種類で分ける
	if (target->type_ == COLLIDER_CIRCLE)
	{
		return IsHitBoxVsCircle((BoxCollider*)target, this);
	}
	else
	{
		return IsHitCircleVsCircle((SphereCollider*)target, this);
	}
	return false;
}
