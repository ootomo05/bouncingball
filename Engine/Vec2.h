#pragma once

//二次元ベクトルを管理するクラス
struct Vec2
{
	float x;
	float y;

	Vec2() = default;
	Vec2(float x, float y) :
		x(x), y(y)
	{
	}

	Vec2 Dot(const Vec2& a);

	Vec2 Cross(const Vec2& a);

	Vec2 Length();

	Vec2 Normalize();

	Vec2 operator+(const Vec2& a) { return Vec2(this->x + a.x, this->y + a.y); };
	Vec2 operator-(const Vec2& a) { return Vec2(this->x - a.x, this->y - a.y); };
	Vec2 operator*(float a) { return Vec2(this->x * a, this->y * a); };
	Vec2 operator/(float a) { return Vec2(this->x / a, this->y / a); };

	const unsigned int GetUIntX() const{ return unsigned int(x); }
	const unsigned int GetUIntY() const{ return unsigned int(y); }
};

