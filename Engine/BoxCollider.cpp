#include "BoxCollider.h"
#include "SphereCollider.h"

//コンストラクタ
BoxCollider::BoxCollider(XMVECTOR basePos, XMVECTOR size)
{
	center_ = basePos; //箱の位置
	size_ = size; //箱のサイズ
	type_ = COLLIDER_BOX; //コライダーの種類を決定
}

//衝突判定
bool BoxCollider::IsHit(Collider * target)
{
	//コライダーの種類によって分ける
	if (target->type_ == COLLIDER_BOX)
	{
		return IsHitBoxVsBox((BoxCollider*)target, this); //ボックス同士
	}
	else
	{
		return IsHitBoxVsCircle(this, (SphereCollider*)target); //ボックスと球体
	}

	return false; //どちらでもない
}