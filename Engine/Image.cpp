#include "Image.h"
#include <vector>

namespace Image
{
	//データ格納用
	std::vector<ImageData*> imageDatas;
}
	//ファイルのロード
int Image::Lode(std::string fileName)
{
	//イメージデータの登録
	ImageData* pImageData = new ImageData; //作成
	pImageData->pSprite = nullptr; //初期化
	pImageData->fileName = fileName; //ファイル名登録

	//同じファイル名がすでに登録されてないか調べる
	for (int i = 0; i < imageDatas.size(); i++)
	{
		if (imageDatas[i]->fileName == fileName)
		{
		//すでにロードされている
		//ロードされているものをそのまま使用
		pImageData->pSprite = imageDatas[i]->pSprite;
		break;
		}
	}

	//まだロードされてない
	if (pImageData->pSprite == nullptr)
	{
		//新しくロードする
		pImageData->pSprite = new Sprite;
		pImageData->pSprite->Initialize(fileName);
	}

	
	imageDatas.push_back(pImageData); //末尾に追加
	//データを格納した場所を返す
	int dataPos = (int)imageDatas.size() - 1;

	ResetRect(dataPos); //初期化

	return dataPos;
}

//変形
void Image::SetTransform(int hImage, Transform & transform)
{
	imageDatas[hImage]->transform = transform; //モデルの変形
}

//描画
void Image::Draw(int hImage)
{
	imageDatas[hImage]->pSprite->Draw(imageDatas[hImage]->transform,imageDatas[hImage]->rect);//描画
}

//後処理
void Image::Relese()
{
	for (int i = 0; i < imageDatas.size(); i++)
	{
		SAFE_DELETE(imageDatas[i]->pSprite);
	}
	imageDatas.clear();
}

//画像を切り取る
void Image::SetRect(int handle, int x, int y, int width, int height)
{
	//配列の範囲外を参照しないようにチェック
	if (handle < 0 || handle >= imageDatas.size())
	{
		return;
	}

	//それぞれの値をセット
	imageDatas[handle]->rect.left = x;
	imageDatas[handle]->rect.top = y;
	imageDatas[handle]->rect.right = width;
	imageDatas[handle]->rect.bottom = height;
}

//初期化を行う
void Image::ResetRect(int handle)
{
	imageDatas[handle]->rect.left = 0;
	imageDatas[handle]->rect.top = 0;
	imageDatas[handle]->rect.right = imageDatas[handle]->pSprite->GetImageWidth();
	imageDatas[handle]->rect.bottom = imageDatas[handle]->pSprite->GetImageHeight();
}

