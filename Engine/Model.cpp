#include "Model.h"
#include "Global.h"

//モデルデータを管理する
namespace Model
{
	std::vector<ModelData*> modelDatas; //新しいモデルがロードされるたびに末尾に追加されてく

	//モデル読み込み
	int Load(std::string fileName)
	{
		//モデルデータの登録
		ModelData* pModelData = new ModelData; //登録先を作成
		pModelData->fileName = fileName;  //ファイル名をを登録

		//すでに同じモデルのファイルがロードされてないか調べる
		for (int i = 0; i < static_cast<int>(modelDatas.size()); i++)
		{
			if (modelDatas[i]->fileName == fileName)
			{
				//すでにロードされてたら、すでに作成したものを共有する
				//pModelData->pFbx = modelDatas[i]->pFbx;
				//break; //同じものを共有できたので探す必要がない
				return i;
			}
		}

		//pModelData->pFbxがnullptrつまり
		//まだロードされてないので新しく作成する
		if (pModelData->pFbx == nullptr)
		{
			pModelData->pFbx = new Fbx;        //fbxクラスを作成
			HRESULT hl = pModelData->pFbx->Load(fileName);  //ファイルのロード
			if (hl)
			{
				return -1;
			}
		}
		modelDatas.push_back(pModelData); //末尾に追加
		
		int dataPos = static_cast<int>(modelDatas.size()) - 1; //配列の要素数は0から数えるので-1する
		return dataPos; //登録した場所を返す
	}

	//モデルの位置、拡大縮小、回転
	//引数 hModel
	//引数 transform
	void SetTransform(int hModel, Transform& transform)
	{
		modelDatas[hModel]->transform = transform; //transformの登録
	}

	//描画
	//引数 hModel
	void Draw(int hModel)
	{
		//fbxクラスのDraw関数を呼んでいるだけ
		modelDatas[hModel]->pFbx->Draw(modelDatas[hModel]->transform);
	}

	//後処理
	void AllDelete()
	{
		//newしたFbxクラスをすべてデリート
		//ループで回すので同じものを指してることもあるので
		//deleteする前は必ずnullptrかどうか調べる
		//SAFE_DELETEを使用してるのでとりあえず必要ない
		int size = static_cast<int>(modelDatas.size());
		for (int i = 0; i < size; i++)
		{
			assert(modelDatas[i]->pFbx != nullptr);
			SAFE_DELETE(modelDatas[i]->pFbx); //newしたのでデリート
		}
		modelDatas.clear(); //使用しないのですべて削除
	}

	//レイを発射する
	void RayCast(int handle, RayCastData *rayData)
	{
		//モデルが変形してもレイが当たるようにする
		//モデルのワールド行列を逆行列にする
		XMMATRIX invWorld = XMMatrixInverse(nullptr, modelDatas[handle]->transform.GetWorldMatrix());


		//レイを撃つ方向にそのままワールド座標の逆行列で変形するとおかしくなるので
		//レイの通過点を求めて通過点を逆行列で変形して方向を求める
		XMVECTOR rayPassPoint = rayData->start + rayData->dir;
		rayPassPoint = XMVector3TransformCoord(rayPassPoint, invWorld);
		
		//レイの発射地点はそのまま逆行列で変形する
		rayData->start = XMVector3TransformCoord(rayData->start, invWorld);

		rayData->dir = rayPassPoint - rayData->start;

		//レイを発射する
		modelDatas[handle]->pFbx->RayCast(rayData);
	}
}