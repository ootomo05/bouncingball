#include "Sprite.h"
#include "Camera.h"

#include "Transform.h"


//コンストラクタ
Sprite::Sprite() : 
	pVertexBuffer_(nullptr), pIndexBuffer_(nullptr), pConstantBuffer_(nullptr),
	indexElementCount_(0), pTexture_(nullptr)
{
}

//デストラクタ
Sprite::~Sprite()
{
}

//初期化
HRESULT Sprite::Initialize(const std::string imgFileName)
{
	//頂点バッファの作成
	if (FAILED(InitVertex()))
	{
		return E_FAIL;
	}

	//インデックスバッファの作成
	if (FAILED(InitIndexBuffer()))
	{
		return E_FAIL;
	}

	//コンスタントバッファの作成
	if (FAILED(InitConstantBuffer()))
	{
		return E_FAIL;
	}

	//画像データを読み込む
	//読み込めたかチェック
	pTexture_ = new Texture;
	if (FAILED(pTexture_->Load(imgFileName)))
	{
		return E_FAIL;
	};
	imgH_ = (float)pTexture_->GetImgHeight();//画像の高さを取得
	imgW_ = (float)pTexture_->GetImagWidth();//画像の幅を取得

	return S_OK;//何も問題なし
}

//頂点バッファの設定
HRESULT Sprite::InitVertex()
{

	// 頂点情報
	//図形をいじるときに変更するとこ
	//頂点座標の設定
	//vertices[]	それぞれの要素は頂点座標、テクスチャの切り取る座標
	//index[]		三角形を作る時の順番指定
	VERTEX vertices[] =
	{
		{XMVectorSet(-1.0f,  1.0f, 0.0f, 0.0f),XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f)},	// 四角形の頂点（左上） 0
		{XMVectorSet(1.0f,  1.0f, 0.0f, 0.0f),XMVectorSet(1.0f, 0.0f, 0.0f, 0.0f)},  	// 四角形の頂点（右上） 1
		{XMVectorSet(1.0f, -1.0f, 0.0f, 0.0f), XMVectorSet(1.0f, 1.0f, 0.0f, 0.0f)},	// 四角形の頂点（右下） 2
		{XMVectorSet(-1.0f, -1.0f, 0.0f, 0.0f), XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f)},	// 四角形の頂点（左下） 3
	};


	// 頂点データ用バッファの設定
	// 一つ一つの頂点が持つ情報をシェーダに渡せるように
	//pVertexBuffer_に設定してやる
	D3D11_BUFFER_DESC bd_vertex;//頂点データ用バッファ用の構造体
	bd_vertex.ByteWidth = sizeof(vertices);
	bd_vertex.Usage = D3D11_USAGE_DEFAULT;
	bd_vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd_vertex.CPUAccessFlags = 0;
	bd_vertex.MiscFlags = 0;
	bd_vertex.StructureByteStride = 0;
	D3D11_SUBRESOURCE_DATA data_vertex;
	data_vertex.pSysMem = vertices;//入れたい情報
	//pVertexBuffer_生成
	if (FAILED(Direct3D::pDevice->CreateBuffer(&bd_vertex, &data_vertex, &pVertexBuffer_)))
	{
		MessageBox(nullptr, "Sprite.cpp：頂点バッファ作成失敗", "エラー", MB_OK);
		return E_FAIL;//:pVertexBuffer_作成でなんか問題あるぞ！
	}

	return S_OK;
}

//インデックスバッファの設定
HRESULT Sprite::InitIndexBuffer()
{
	//インデックス情報
	//作る三角形頂点を結ぶ順番を作る三角形分入れる3つで一組
	//時計回りで指定してやる
	//頂点を増やしたときはここもいじる
	int index[] = { 0,2,3, 0,1,2, 0,4,1 };
	indexElementCount_ = sizeof(index) / sizeof(int);//DrawIndexedの第一引数用

	// インデックスバッファを生成する
	D3D11_BUFFER_DESC   bd;//インデックスバッファ用の構造体
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(index);
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = index;
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;
	//pIndexBuffer_生成
	if (FAILED(Direct3D::pDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer_)))
	{
		MessageBox(nullptr, "Sprite.cpp：インデックスバッファ作成失敗", "エラー", MB_OK);
		return E_FAIL;//:pIndexBuffer_作成でなんか問題あるぞ！
	}


	return S_OK;//全部成功
}

//コンスタントバッファの設定
HRESULT Sprite::InitConstantBuffer()
{
	//コンスタントバッファ作成
	//モデルに付随するカメラやライトの情報を渡す
	D3D11_BUFFER_DESC cb;//コンスタントバッファ用の構造体
	cb.ByteWidth = sizeof(CONSTANT_BUFFER);
	cb.Usage = D3D11_USAGE_DYNAMIC;
	cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cb.MiscFlags = 0;
	cb.StructureByteStride = 0;
	//pConstantBuffer生成
	if (FAILED(Direct3D::pDevice->CreateBuffer(&cb, nullptr, &pConstantBuffer_)))
	{
		MessageBox(nullptr, "Sprite.cpp：コンスタントバッファ作成失敗", "エラー", MB_OK);
		return E_FAIL;//:pConstantBuffer作成でなんか問題あるぞ！
	}
	
	return S_OK;//全部成功
}

//描画
void Sprite::Draw(Transform& transform,RECT rect)
{
	//使うシェーダの種類を決める
	//Spriteクラスは2Dを扱うので2Dのシェーダを使う
	Direct3D::SetShaderBundle(SHADER_2D);

	//コンスタントバッファの値の設定
	SetConstantBuffer(transform,rect);

	//テクスチャとサンプラーをシェーダに渡す処理
	ID3D11SamplerState* pSampler = pTexture_->GetSampler();
	Direct3D::pContext->PSSetSamplers(0, 1, &pSampler);

	ID3D11ShaderResourceView* pSRV = pTexture_->GetSRV();
	Direct3D::pContext->PSSetShaderResources(0, 1, &pSRV);

	Direct3D::pContext->Unmap(pConstantBuffer_, 0);	//再開

	//頂点バッファ
	UINT stride = sizeof(VERTEX);
	UINT offset = 0;
	Direct3D::pContext->IASetVertexBuffers(0, 1, &pVertexBuffer_, &stride, &offset);

	// インデックスバッファーをセット
	stride = sizeof(int);
	offset = 0;
	Direct3D::pContext->IASetIndexBuffer(pIndexBuffer_, DXGI_FORMAT_R32_UINT, 0);

	//コンスタントバッファ
	Direct3D::pContext->VSSetConstantBuffers(0, 1, &pConstantBuffer_);	//頂点シェーダー用	
	Direct3D::pContext->PSSetConstantBuffers(0, 1, &pConstantBuffer_);	//ピクセルシェーダー用

	Direct3D::pContext->DrawIndexed(indexElementCount_, 0, 0);//描画
}

void Sprite::SetConstantBuffer(Transform& transform,RECT rect)
{

	float winW = (float)Direct3D::winWidth; //幅
	float winH = (float)Direct3D::winHeight;//高さ
	
	//画像のサイズをもとのサイズで描画する 画像サイズ幅 / ウィンドウサイズ幅 , 画像サイズ高さ / ウィンドウサイズ高さ
	//XMMATRIX ajustScale = XMMatrixScaling(imgW_,imgH_, 1);

	//画面の幅、高さの割合
	//XMMATRIX miniScale = XMMatrixScaling(1.0f / winW,1.0f / winH,1);

	//コンスタントバッファに渡す情報
	//今のところカメラ情報のみ渡す
	//XMMatrixTranspose()シェーダ用行列に変換してくれる関数
	CONSTANT_BUFFER cb;
	
	XMMATRIX cut = XMMatrixScaling((float)rect.right, (float)rect.bottom, 1);
	XMMATRIX view = XMMatrixScaling(1.0f / Direct3D::winWidth, 1.0f / Direct3D::winHeight, 1.0f);
	XMMATRIX world = cut * transform.matScale_ * transform.matRotate_ * view * transform.matTranslate_;
	//cb.matW = XMMatrixTranspose(ajustScale * transform.matScale_ * transform.matRotate_ * miniScale * transform.matTranslate_); //ピクセル単位で動かしたい時使う
	cb.matW = XMMatrixTranspose(world);

	//テクスチャから切り取る部分を指定する
	XMMATRIX mTexTrans = XMMatrixTranslation((float)rect.left / (float)pTexture_->GetImagWidth(),
		(float)rect.top / (float)pTexture_->GetImgHeight(),0.0f);
	XMMATRIX mTexScale = XMMatrixScaling((float)rect.right / (float)pTexture_->GetImagWidth(),
		(float)rect.bottom / (float)pTexture_->GetImgHeight(), 1.0f);
	XMMATRIX mTexel = mTexScale * mTexTrans;
	cb.uvTrans = XMMatrixTranspose(mTexel);

	D3D11_MAPPED_SUBRESOURCE pdata;
	Direct3D::pContext->Map(pConstantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata);	// GPUからのデータアクセスを止める
	memcpy_s(pdata.pData, pdata.RowPitch, (void*)(&cb), sizeof(cb));                    // データを値を送る
}

//開放
void Sprite::AllDelete()
{
	//Releaseの代わりにマクロを使用して開放処理をする
	//pTexture_->Release();
	SAFE_RELEASE(pTexture_);
	SAFE_RELEASE(pConstantBuffer_);
	SAFE_RELEASE(pIndexBuffer_);
	SAFE_RELEASE(pVertexBuffer_);
}

int Sprite::GetImageWidth()
{
	return pTexture_->GetImagWidth();
}

int Sprite::GetImageHeight()
{
	return pTexture_->GetImgHeight();
}
