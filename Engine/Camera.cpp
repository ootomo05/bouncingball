#include "Camera.h"
#include "Direct3D.h"

namespace Camera
{
	//変数
	XMVECTOR position_;	//カメラの位置（視点）
	XMVECTOR target_;	//見る位置（焦点）
	XMMATRIX viewMatrix_;	//ビュー行列
	XMMATRIX projMatrix_;	//プロジェクション行列

	const float VIEW_NEAR = 0.1f;//手前
	const float VIEW_FAR = 100.0f;//どこまで見えるか

	//初期化
	void Initialize()
	{
		position_ = XMVectorSet(0, 3.0f, -10.0f, 0);	//カメラの位置
		target_ = XMVectorSet(0, 0, 0, 0);	//カメラの焦点

		//ゲーム中にいじりたいときupdate関数にこの処理を書く
		//プロジェクション行列
		projMatrix_ = XMMatrixPerspectiveFovLH(XM_PIDIV4, static_cast<FLOAT>(800) / static_cast<FLOAT>(600), VIEW_NEAR, VIEW_FAR);
	}

	//更新
	void Update()
	{
		//ビュー行列の作成
		viewMatrix_ = XMMatrixLookAtLH(position_, target_, XMVectorSet(0, 1.0f, 0, 0));
	}

	//位置を設定
	void SetPosition(XMVECTOR position)
	{
		position_ = position;
	}
	XMVECTOR GetTarget() { return target_; }
	//焦点を設定
	void SetTarget(XMVECTOR target)
	{
		target_ = target;
	}

	XMVECTOR GetPosition()
	{
		return position_;
	}

	//ビュー行列を取得
	XMMATRIX GetViewMatrix()
	{
		return viewMatrix_;
	}

	//プロジェクション行列を取得
	XMMATRIX GetProjectionMatrix()
	{
		return projMatrix_;
	}

}