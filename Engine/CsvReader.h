#pragma once
#include <vector>
#include <string>

//CSV読み込みを管理するクラス
class CsvReader
{
	std::vector<std::vector<std::string>> data_; //ファイルから読み込んだデータを格納する変数

	//コンマ区切りにする
	//引数 result 
	//引数 data
	//引数 index
	void GetToComma(std::string *result, std::string data, unsigned long* index);
public:

	CsvReader();
	~CsvReader();

	//CSVファイルを読み込む
	//引数 fileName 読み込むファイル名
	//戻値 読み込みに成功したらtrueを返す
	bool Load(std::string fileName);

	//ファイルからデータを読み込む
	//引数 x
	//引数 y
	//戻値 
	int GetValue(const unsigned int x,const unsigned int y);

	std::string GetString(const unsigned int x,const  unsigned int y);

	//読み込んだファイルの行数を取得
	//引数 なし
	//戻値 行数
	size_t GetWidth();

	//読み込んだファイルの列数を取得
	//引数 なし
	//戻値 ファイルの列数
	size_t GetHeight();
};

