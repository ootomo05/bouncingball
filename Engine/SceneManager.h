#pragma once
#include "GameObject.h"

//シーンの種類
enum SCENE_ID
{
	SCENE_ID_TEST = 0, //テスト用シーン
	SCENE_ID_TITLE,    //タイトルシーン
	SCENE_ID_GAME,     //実際にゲームを処理するシーン
	SCENE_ID_RESULT    //リザルトシーン
};

//シーンを管理するクラス
class SceneManager : public GameObject
{
	SCENE_ID nextSceneID_; //現在のシーン
	SCENE_ID nowSceneID_;  //切り替えたいシーン
public:

	//コンストラクタ
	//引数 親オブジェクト
	SceneManager(GameObject* parent);

	//初期化
	//引数 なし
	void Initialize() override;

	//更新
	//引数 なし
	void Update() override;

	//描画
	//引数 なし
	void Draw() override;

	//開放
	//引数 なし
	void Release() override;

	//次のシーンに切り替える
	//引数 next 切り替えたいシーンのid
	void ChangeScene(SCENE_ID next);
};

