#pragma once
#include "Collider.h"
class BoxCollider : public Collider
{
	friend class Collider;
public:

	//コンストラクタ
	//引数 basePos モデルから見たコライダーの位置
	//引数 size コライダーのサイズ x幅、y高さ、z奥行
	BoxCollider(XMVECTOR basePos,XMVECTOR size);

private:

	//衝突判定
	//引数 target 衝突しているか調べたい相手
	//戻値 衝突していればtrueを返す
	bool IsHit(Collider * target) override;
};

