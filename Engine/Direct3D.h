#pragma once

//インクルード
#include <d3d11.h>
#include <assert.h>
#include "Global.h"

//リンカ
//コンパイルするときにファイルを読み込むように
//指定する
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")



//シェーダを増やしたとき要素をふやす
enum SHADER_TYPE
{
	SHADER_3D = 0,//3Dシェーダを使う
	SHADER_2D,	  //2Dシェーダを使う
	SHADER_MAX    //登録されているシェーダの最大値
};

namespace Direct3D
{
	extern ID3D11Device*           pDevice; //ほかでも使いたい
	extern ID3D11DeviceContext*    pContext;//デバイスコンテキスト
	
	extern UINT winWidth; //ウィンドウ横幅
	extern UINT winHeight;//ウィンドウ高さ

	
	//DirectXの初期化
	//引数	winW	ウィンドウの幅
	//引数	winH	ウィンドウの高さ
	//引数	hWnd	ウィンドウのハンドル
	//戻り値 成功か失敗
	HRESULT Initialize(int winW, int winH, HWND hWnd);

	//シェーダの初期化
	//引数	なし
	//戻り値	成功か失敗
	HRESULT InitShader();


	//3Dシェーダの準備
	//引数   なし
	//戻り値 成功か失敗
	HRESULT InitShader3D();


	//2Dシェーダの作成
	//引数   なし
	//戻り値 成功か失敗
	HRESULT InitShader2D();


	//使うシェーダの種類を設定 Draw関数などで呼び出す
	//引数   type  使うシェーダの種類
	//戻り値 なし
	void SetShaderBundle(SHADER_TYPE type);
	

	//描画開始
	//引数	なし
	//戻り値	なし
	void BeginDraw();


	//描画終了
	//引数	なし
	//戻り値	なし
	void EndDraw();


	//解放
	//引数	なし
	//戻り値	なし
	void Release();
};