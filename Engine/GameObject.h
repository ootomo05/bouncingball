#pragma once

#include <list>
#include <vector>
#include <string>
#include "Transform.h"
#include "Global.h"

//プロトタイプ宣言
class Collider;

//ゲームに出てくるすべてのオブジェクトの基
class GameObject
{
	//親もGameObject型なので、
	//アドレスを入れる
	std::string	objectName_;           //オブジェクトの名前
	bool isDead_ = false; //削除用フラグ

protected:
	GameObject*	pParent_;              //オブジェクトの親
	Transform	transform_;            //位置、回転、サイズの情報

	//子は増えていくのでリスト構造にする
	std::list<GameObject*> childList_; //オブジェクトの子供
	Collider* colliders_; //オブジェクトに登録されるコライダーへのアクセス先を保持
public:
	//デフォルトコンストラクタ
	GameObject();

	//コンストラクタ
	//引数 parent オブジェクトの親
	//引数 name オブジェクトの名前
	GameObject(GameObject* parent, const std::string& name);

	//デストラクタ
	virtual ~GameObject();
	
	//初期化
	//引数 なし
	virtual void Initialize() = 0;

	//更新
	//引数 なし
	virtual void Update() = 0;

	//描画
	//引数 なし
	virtual void Draw() = 0;

	//描画
	//引数 なし
	virtual void Release() = 0;

	//子供すべてを描画
	//引数 なし
	void DrawSub();

	//すべての子供の更新
	//引数 なし
	void UpdateSub();

	//すべての子供の開放
	//引数 なし
	void ReleaseSub();


	//オブジェクトの名前を返す
	//引数 なし
	//戻り値 オブジェクトの名前
	std::string GetObjectName() { return objectName_; }

	//各Transform型変数へのアクセッサ
	//位置座標をセット
	//引数 position 位置
	void SetPosition(DirectX::XMVECTOR& position);

	//大きさをセット
	//引数 scale 倍率
	void SetScale(DirectX::XMVECTOR& scale);

	//回転させる
	//引数 rotate 回転量
	void SetRotate(DirectX::XMVECTOR& rotate);

	//オブジェクトの座標を取得
	//引数 なし
	//戻り値 オブジェクトの座標
	DirectX::XMVECTOR GetPosition();

	//オブジェクトの今の大きさを取得
	//戻り値 現在の大きさ
	DirectX::XMVECTOR GetScale();

	DirectX::XMVECTOR GetWorldRotate();

	DirectX::XMVECTOR GetWorldScale();

	//オブジェクトの回転量を取得
	//戻り値 回転量
	DirectX::XMVECTOR GetRotate();

	DirectX::XMVECTOR GetWorldPosition();

	//削除フラグを立てる
	//引数 なし
	void KillMe();

	//死んだことを通知する
	//引数 なし
	//戻り値 死んでいたらtrueを返す
	bool IsDead();
	
	//子供とコライダーをすべて削除
	//引数 なし
	void DeleteAllChildren();

	//自分の子供の中から探す
	//引数 name 探したいオブジェクトの名前
	//戻り値 オブジェクトが作成されているアドレス
	GameObject* FindChildObject(const std::string name);
	
	//rootjobをを取得
	//引数 なし
	//戻り値 RootJobが作られているアドレス
	GameObject* GetRootJob();

	//特定のオブジェクトを探す
	//引数 name 探したいオブジェクトの名前
	//戻り値 見つけたオブジェクトのアドレス
	GameObject* FindObject(const std::string name);
	
	GameObject* GetParent();

	//当たり判定
	//引数 pTarget 当たったか調べたいオブジェクト
	void Collision(GameObject* pTarget);

	//オブジェクトにコライダーをつける
	//引数 collider オブジェクトにつけるコライダーの情報
	void AddCollider(Collider* collider);

	//コライダー同士の衝突が起こった時に自動で実行される
	//引数 pTareget 自分のコライダーと当たったコライダーを持っているゲームオブジェクト
	void virtual OnCollision(GameObject* pTarget);

	//子を追加する
	//引数 obj 追加する子オブジェクト
	void PushBackChild(GameObject* obj);
	

	//何度も書くのは面倒なので
	//GameObjectを継承してる奴なら
	//どこでも使えるようにする
	//型を後から設定する
	template <class T>
	//オブジェクトを生成して
	//生成したオブジェクトを子どもにしてくれる
	//引数 parent 親クラス
	//戻り値 作ったオブジェクトのアドレス
	GameObject* Instantiate(GameObject* parent)
	{
		T* p;                    //子供のクラス型ポインタを宣言
		p = new T(parent);       //子供を生成
		
		if (parent != nullptr)
		{
			parent->PushBackChild(p);
		}

		p->transform_.pParent_ = &parent->transform_;
		//先に子を追加してからInitializeを呼ばないといけない
		p->Initialize();         //子供の初期化を行う
		return p;
	}
};
