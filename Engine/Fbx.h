#pragma once


//インクルード
#include <d3d11.h>
#include <fbxsdk.h>
#include <string>
#include "Transform.h"


//リンカ
#pragma comment(lib, "LibFbxSDK-MT.lib")
#pragma comment(lib, "LibXml2-MT.lib")
#pragma comment(lib, "zlib-MT.lib")

//プロトタイプ宣言
class Texture;

//レイキャスト用構造体
struct RayCastData
{
	XMVECTOR	start;	//レイ発射位置
	XMVECTOR	dir;	//レイの向きベクトル
	float		dist;	//衝突点までの距離
	BOOL        hit;	//レイが当たったか
};

//fbxファイルを扱うクラス
class Fbx
{
	//マテリアルから取得する情報の種類
	struct MATERIAL
	{
		Texture*	pTexture; //テクスチャ情報
		XMFLOAT4	diffuse;  //マテリアルの色
	};

	//シェーダに渡す情報
	//頂点場以外で必要な情報
	struct CONSTANT_BUFFER
	{
		XMMATRIX	matWVP;  //ローカル座標を
		XMMATRIX	matW;    //ワールド行列
		XMFLOAT4	diffuseColor;	// ディフューズカラー（マテリアルの色）
		int		isTexture;		// テクスチャ貼ってあるかどうか intにしたら櫛に色がついた！
	};

	
	
	//一つの頂点に持たせる情報
	struct VERTEX
	{
		XMVECTOR position;  //モデルの頂点座標
		XMVECTOR uv;        //画像の頂点情報
		XMVECTOR normal;	//法線情報
	};

	int vertexCount_;	//頂点数
	int polygonCount_;	//ポリゴン数
	int materialCount_;	//マテリアルの個数
	int* pIndexCountEachMaterial_; //同じマテリアルが使われているポリゴンの総頂点数
	int** ppIndex_;

	ID3D11Buffer *pVertexBuffer_;    //頂点バッファ
	ID3D11Buffer **ppIndexBuffer_;    //マテリアルごとに頂点を分けてやる
	ID3D11Buffer *pConstantBuffer_;  //コンスタントバッファ
	MATERIAL *pMaterialList_;        //マテリアルのデータを登録する
	VERTEX* pVertices_;//頂点情報を入れる配列

	//頂点バッファの情報
	//引数 mesh 頂点情報を抜き出したいメッシュ
	//戻り値 成功か失敗
	HRESULT InitVertex(fbxsdk::FbxMesh * mesh);

	//インデックス情報の取得
	//引数 mesh メッシュ情報
	//戻り値 成功か失敗
	HRESULT InitIndex(fbxsdk::FbxMesh * mesh);

	//コンスタントバッファの準備
	//引数 なし
	//戻り値 成功か失敗
	HRESULT InitConstantBuffer();

	//マテリアルの情報を取得
	//引数 pNode ノードの情報
	//戻り値 成功か失敗
	HRESULT InitMaterial(fbxsdk::FbxNode * pNode);

public:

	Fbx();
	~Fbx();

	//fbxファイルを読み込む
	//引数   fileName モデルのファイル名
	//戻り値 成功か失敗
	HRESULT Load(std::string fileName);

	//読み込んだファイルを描画
	//引数 transform 描画位置
	void    Draw(Transform& transform);

	//レイとの衝突判定を行う
	//引数 rayData レイを飛ばすための情報
	void RayCast(RayCastData * rayData);


	//開放処理
	//引数 なし
	void    Release();
};