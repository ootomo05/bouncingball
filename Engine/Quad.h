#pragma once

#include <DirectXMath.h>
#include <string>
#include "Direct3D.h"
#include "Texture.h"
using namespace DirectX;

class Transform;

class Quad
{
protected:
	/*
	~~~~継承先だけに見せたい変数や
	関数をの宣言を記述~~~~
	*/

	//コンスタントバッファー
	struct CONSTANT_BUFFER
	{
		XMMATRIX	matWVP;//ワールド、ビュー、プロジェクション
		XMMATRIX    matW;  //ワールド行列
	};

	//頂点情報
	struct VERTEX
	{
		XMVECTOR position;//頂点情報
		XMVECTOR uv;      //uv座標情報
		XMVECTOR normal;  //法線ベクトル
	};

protected:
	//1,2,3の順に実態を作るので領域を解放するときは逆の順にする
	ID3D11Buffer *pVertexBuffer_;	//頂点バッファ	1
	ID3D11Buffer *pIndexBuffer_;	//	2
	ID3D11Buffer *pConstantBuffer_;	//コンスタントバッファ 3
	//テクスチャを扱うクラスへアクセスするポインタ
	Texture* pTexture_;
	int indexElementCount_;//index[]の容量がいくらになるかを入れる
	
	//頂点バッファの初期化
	//変更しないなら継承先でそのまま使う
	//引数   なし
	//戻り値 成功か失敗
	HRESULT InitVertexBuffer();

	//インデックスバッファの初期化
	//変更しないなら継承先でそのまま使う
	//引数   なし
	//戻り値 成功か失敗
	HRESULT InitIndexBuffer();

	//コンスタントバッファの初期化
	//変更しないなら継承先でそのまま使う
	//引数   なし
	//戻り値 成功か失敗
	HRESULT InitConstantBuffer();
public:

	/*
	~~~~全員に見せる変数や関数記述~~~~
	*/
	//コンストラクタ
	Quad();

	//デストラクタ
	~Quad();
	
	//初期化
	//引数	imgFilePass 実行ファイルからの画像ファイルへの相対パス
	//戻り値	成功か失敗
	virtual HRESULT Initialize(std::string imgFileName);

	
	//描画
	//引数	transform　移動、向き、拡大縮小をまとめたもの
	//戻り値	なし
	void Draw(Transform& transform);

	//開放
	//引数	なし
	//戻り値	なし
	void Release();

};