#pragma once
#include <string>
#include "Transform.h"
#include "Sprite.h"

//画像を管理する
namespace Image
{
	struct ImageData
	{
		std::string fileName; //画像ファイル名
		Transform transform; //変形
		Sprite* pSprite; //画像をポリゴンに張り付ける
		RECT rect;       //画像を切り取るときに使う
	};

	//画像のロード
	//引数 fileName 読み込みたいファイル
	//戻り値 登録場所
	int Lode(std::string fileName);

	//変形させる
	//引数 hImage 変形したい画像のアクセス場所
	//引数 transform 変形
	void SetTransform(int hImage,Transform& transform);

	//描画
	//引数 hImage 描画したい画像の場所
	void Draw(int hImage);

	//確保した素材の開放
	//引数 なし
	void Relese();

	//画像の切り取り
	//引数 handle 切り取る画像番号
	//引数 x 切り取り始めるx座標
	//引数 y 切り取り始めるy座標
	//引数 width 切り取る幅
	//引数 height 切り取る高さ
	void SetRect(int handle, int x, int y, int width, int height);

	//画像切り取りの初期化
	void ResetRect(int handle);
};

