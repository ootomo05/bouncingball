#include "DestWall.h"
#include "Engine/Model.h"
#include "Engine/BoxCollider.h"

//コンストラクタ
DestWall::DestWall(GameObject * pParent)
	: GameObject(pParent,"DestWall"), hModel_(-1)
{
}

//デストラクタ
DestWall::~DestWall()
{
}

//初期化
void DestWall::Initialize()
{
	//読み込み
	hModel_ = Model::Load("Assets\\DestWall.fbx");
	assert(hModel_ >= 0);

	//コライダーの設定
	BoxCollider* collision = new BoxCollider(XMVectorSet(-0.5f, 0.5f, 0.5f, 0), XMVectorSet(1.0f, 1.0f, 1.0f, 0));
	AddCollider(collision);
}

//更新
void DestWall::Update()
{
}

//描画
void DestWall::Draw()
{
	//描画
	Model::SetTransform(hModel_,transform_);
	Model::Draw(hModel_);
}

//解放
void DestWall::Release()
{
}