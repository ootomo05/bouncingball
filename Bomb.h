#pragma once
#include "Engine/GameObject.h"

//爆弾を管理するクラス
class Bomb : public GameObject
{
	//２次元ベクトル
	struct Vec2
	{
		float x; //x座標
		float y; //y座標
	};

	const Vec2 DIRECTIONS[4] =
	{
		{0.0f,1.0f},   //上
		{0.0f,-1.0f},  //下
		{1.0f,0.0f},  //右
		{-1.0f,0.0f}, //左
	};

	int hModel_; //モデル番号
	const float BLAST_TIME = 60.0f * 3.0f; //3秒後爆破
	float timeCount_; //時間を計測
	bool tFlag = true;

public:

	//コンストラクタ
	//引数 pParent 親
	Bomb(GameObject* pParent);

	//デストラクタ
	~Bomb();

	//初期化
	//引数 なし
	virtual void Initialize() override;

	//更新
	//引数 なし
	virtual void Update() override;

	void DoBlast();

	//描画
	//引数 なし
	virtual void Draw() override;

	//開放
	//引数 なし
	virtual void Release() override;
};

