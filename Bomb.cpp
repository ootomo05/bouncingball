#include "Bomb.h"
#include "Engine/Model.h"
#include "Blast.h"

//コンストラクタ
Bomb::Bomb(GameObject * pParent) :
	GameObject(pParent,"Bomb"),hModel_(-1), timeCount_(0)
{
}

//デストラクタ
Bomb::~Bomb()
{
}

//初期化
void Bomb::Initialize()
{
	//モデルのロード
	hModel_ = Model::Load("Assets\\Bomb.fbx");
	assert(hModel_ >= 0);
}

//更新
void Bomb::Update()
{
	DoBlast(); //爆発させる
}

void Bomb::DoBlast()
{
	//static bool tFlag = true;
	if (timeCount_ > BLAST_TIME && tFlag)
	{
		GameObject* parent = GetParent();
		tFlag = false;

		for (UINT i = 0; i < 4; i++)
		{
			Blast* pBlast = (Blast*)Instantiate<Blast>(GetParent());
			XMVECTOR pos = transform_.position_;
			pBlast->SetPosition(pos);
			pBlast->SetBasePos(pos.vecX, pos.vecZ);
			pBlast->SetMoveVec(XMVectorSet(DIRECTIONS[i].x, 0.0f, DIRECTIONS[i].y, 1.0f));
		}
		KillMe();
	}
	timeCount_++; //
}

//描画
void Bomb::Draw()
{
	//モデルの描画
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}



//開放
void Bomb::Release()
{
}
