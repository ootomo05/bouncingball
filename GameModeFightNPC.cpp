#include "GameModeFightNPC.h"
#include "Engine/SceneManager.h"

// コンストラクタ
GameModeFightNPC::GameModeFightNPC(GameObject* parent) : GameMode(parent)
{
}

// デストラクタ
GameModeFightNPC::~GameModeFightNPC()
{
}

// ゲーム開始
void GameModeFightNPC::StartGame()
{
	SceneManager* pSceneManager = (SceneManager*)parent_->FindObject("SceneManager");
	pSceneManager->ChangeScene(SCENE_ID_GAME);
}