#pragma once
#include "Engine/GameObject.h"

//ゲームスコアを管理するクラス
class Score : public GameObject
{
	int hScoreImage_;
public:
	Score(GameObject* parent);
	~Score();

	// GameObject を介して継承されました
	virtual void Initialize() override;
	virtual void Update() override;
	virtual void Draw() override;
	virtual void Release() override;
};

