#include "Blast.h"
#include "Engine/Model.h"
#include "Stage.h"
#include "Engine/SphereCollider.h"

//コンストラクタ
Blast::Blast(GameObject * pParent) : 
	GameObject(pParent,"Blast"),hModel_(-1)
{
}

Blast::~Blast()
{
}

void Blast::Initialize()
{
	//モデルの読み込み
	hModel_ = Model::Load("Assets\\Blast.fbx");
	assert(hModel_ >= 0);

	//コライダーの設定
	SphereCollider* collision = new SphereCollider(XMVectorSet(0, 0, 0, 0), 0.3f);
	AddCollider(collision);
}

//更新
void Blast::Update()
{
	Move(); //移動
}

//移動
void Blast::Move()
{
	transform_.position_ += moveVec_ * SPEED;

	XMVECTOR pos = XMVectorSet(baseVec.x, 0.0f, baseVec.y, 0.0f);
	pos = pos - transform_.position_;
	float leng = XMVector3Length(pos).vecX;
	if (leng > KILL_LENGE)
	{
		KillMe();
	}
}

//何かに当たった
void Blast::OnCollision(GameObject * pTarget)
{
	//当たったときの処理
	if (pTarget->GetObjectName() == "DestWall")
	{
		XMVECTOR targetPos = pTarget->GetPosition();
		Stage* pStage = (Stage*)FindObject("Stage");
		pStage->SetMapData(targetPos.vecX, targetPos.vecZ, 0);

		//相手と自分を殺す
		pTarget->KillMe();
		KillMe();
	}
}

//描画
void Blast::Draw()
{
	//モデルの描画
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Blast::Release()
{
}
