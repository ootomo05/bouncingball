#pragma once
#include "Engine/GameObject.h"
class Player : public GameObject
{
	int hModel_; //モデルを扱う変数
	float gravity_; //重力
	XMVECTOR moveVec_; //移動ベクトル
	const float MODEL_RADIUS = 0.250f;

	float FClamp(const float val,const float min,const float max);
public:
	Player(GameObject* parent);
	~Player();

	// GameObject を介して継承されました
	virtual void Initialize() override;
	virtual void Update() override;

	//テスト用関数
	//動きのテスト
	void TestMove();

	void HitWall(XMVECTOR & pos, const float radius);
	virtual void Draw() override;
	virtual void Release() override;

	void OnCollision(GameObject* pTarget);
};

