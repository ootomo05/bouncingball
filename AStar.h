#pragma once
#include <list>
#include "Engine/Vec2.h"

//前方宣言
class AStarNode;
class Stage;

//AStarアルゴリズムを管理する
class AStar
{
	const uint32_t MAP_WIDTH; //幅
	const uint32_t MAP_HEIGHT; //高さ

	Vec2 goalPos_; //ゴール

	std::list<AStarNode*> pNodeOpenList_; //オープンリスト
	AStarNode*** pppNodes_; //ノード

	bool IsRunAStar_; //AStarアルゴリズムを実行できるかフラグ

	//配列を探査するときの方向
	Vec2 direcions[4] = 
	{
		{-1,0 }, //上
		{1,0},  //下
		{0,-1}, //左
		{0,1}   //右
	};
	std::list<Vec2> shortestPath_; //最短距離を格納

public:

	//コンストラクタ(ノードを作成するための準備)
	//引数 width  マップの幅
	//引数 heigth マップの高さ
	AStar(uint32_t width,uint32_t heigth);

	//デストラクタ
	~AStar();

	//初期化
	//引数 goalPos ゴール座標
	//引数 stage   Stageクラスのインスタンス
	void Init(const Vec2& goalPos,Stage* stage);

	//最短経路を探査する
	//引数 startPos 探査スタート地点
	void Algorithm(const Vec2& startPos);

	//最短経路をセットする
	//引数 pNode 親ノード
	void ShortPath(AStarNode* pNode);

	//次の座標を取得する
	//引数 なし
	//戻値 最短経路の座標(リストの中身がからなら-1を返す)
	Vec2 GetNextPoint();
};