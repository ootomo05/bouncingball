#include "Stage.h"
#include "Engine/Model.h"
#include "Engine/CsvReader.h"
#include "Engine/Camera.h"
#include "DestWall.h"

//コンストラクタ
Stage::Stage(GameObject * pParent) :
	GameObject(pParent,"Stage"), ppMapData_(nullptr),mapWidht(0),mapHeiht(0)
{
	memset(hCellModels_, -1, sizeof(hCellModels_)); //初期化
	cost_.clear();
}

//デストラクタ
Stage::~Stage()
{
}

//初期化
void Stage::Initialize()
{
	//新しくモデルを追加するときは
	//ファイル名をここに登録する
	std::string fileNameList[CELL_TYPE_MAX] =
	{
		{"assets\\Floor.fbx"},
		{"assets\\Wall.fbx"},
		{"assets\\RedFloor.fbx"},
		{"assets\\GreenFloor.fbx"}
	};
	for (UINT i = 0; i < CELL_TYPE_MAX; i++)
	{
		//モデル読み込み
		hCellModels_[i] = Model::Load(fileNameList[i]);
		assert(hCellModels_[i] >= 0);
	}

	//各セルのコストを設定
	cost_[CELL_TYPE_ROAD] = 0.3f;
	cost_[CELL_TYPE_WALL] = -10.0f;
	cost_[CELL_TYPE_RED_FLOOR] = 0.4f;
	cost_[CELL_TYPE_GREEN_FLOOR] = 0.1f;

	//csvファイルから読み込む
	CsvReader csv;
	csv.Load("MapData.csv");
	mapWidht = csv.GetWidth();
	mapHeiht = csv.GetHeight();

	//マップ配列の作成 幅＊高さ
	ppMapData_ = new int*[mapWidht];
	for (UINT i = 0; i < mapWidht; i++)
	{
		ppMapData_[i] = new int[mapHeiht];
	}

	//csvファイルからマップデータの読み込み
	for (UINT x = 0; x < mapWidht; x++)
	{
		for (UINT y = 0; y < mapHeiht; y++)
		{
			ppMapData_[x][y] = csv.GetValue(x, y);

		}
	}

	//ステージが中央になるようにカメラを設定
	Camera::SetTarget(XMVectorSet(mapWidht / 2.0f,0.0f,5.0f, 0));
	Camera::SetPosition(XMVectorSet(mapWidht / 2.0f,10.0f,-5.0f, 0));

}

//更新
void Stage::Update()
{
}

//描画
void Stage::Draw()
{
	//マップの描画
	for (UINT x = 0; x < mapWidht; x++)
	{
		for (UINT z = 0; z < mapHeiht; z++)
		{
			//モデルの描画場所を設定
			Transform trans;
			trans.position_.vecX = (float)x;
			trans.position_.vecZ = (float)z;
			trans.Calclation();

			//モデルの描画
			Model::SetTransform(hCellModels_[ppMapData_[x][z]], trans);
			Model::Draw(hCellModels_[ppMapData_[x][z]]);
		}
	}
}

//解放
void Stage::Release()
{
}

int Stage::GetMapData(const unsigned int x, const unsigned int y)
{
	return ppMapData_[x][y];
}

//コストを取得
int Stage::GetCost(CELL_TYPE type)
{
	return cost_[type];
}

