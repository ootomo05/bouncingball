#include "Player.h"
#include "Engine/Model.h"
#include "Engine/Camera.h"
#include "Engine/SphereCollider.h"
#include "Engine/Input.h"
#include "Stage.h"
#include "Bomb.h"
#include "Engine/SceneManager.h"

//float型クランプ
float Player::FClamp(const float val, const float min, const float max)
{
	if (val < min)
	{
		return min;
	}
	else if (val > max)
	{
		return max;
	}
	
	return val;
}

//コンストラクタ
Player::Player(GameObject* parent)
	: GameObject(parent,"Player"), hModel_(-1),gravity_(-1.0f)
{
	//テスト用
	moveVec_ = XMVectorSet(0, 0, 0, 0);
	
	//半径が1の球体コライダーをセット
	SphereCollider* collision = new SphereCollider(XMVectorSet(0, 0, 0, 0), 1);
	AddCollider(collision);
}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{
	//モデル読み込み
	hModel_ = Model::Load("Assets\\Player.fbx");
	assert(hModel_ >= 0);

	//初期位置
	transform_.position_.vecX = 1.0f;
	transform_.position_.vecZ = 1.0f;
}

//更新
void Player::Update()
{
	TestMove(); //移動
}

//移動
void Player::TestMove()
{
	moveVec_ = XMVectorSet(0, 0, 0, 0);

	//WASDで上下左右移動
	if (Input::IsKey(DIK_W))moveVec_.vecZ = 1;
	if (Input::IsKey(DIK_A))moveVec_.vecX = -1;
	if (Input::IsKey(DIK_S))moveVec_.vecZ = -1;
	if (Input::IsKey(DIK_D))moveVec_.vecX = 1;

	moveVec_ = XMVector3Normalize(moveVec_); //長さが1の方向ベクトルにする

	transform_.position_ += moveVec_ * 0.3f; //座標を変更

	HitWall(transform_.position_,MODEL_RADIUS); //壁との当たり判定を行う


	//床の色を変更する
	Stage* pStage = (Stage*)FindObject("Stage");
	assert(pStage != nullptr);

	//座標をunsigned int型にキャスト
	const unsigned int curentPosX = (unsigned int)transform_.position_.vecX;
	const unsigned int curentPosZ = (unsigned int)transform_.position_.vecZ;
	
	//座標の床の色を変更
	pStage->SetColorFloor(curentPosX, curentPosZ, CELL_TYPE_RED_FLOOR);
}

//壁に当たった時の処理
void Player::HitWall(XMVECTOR& pos,const float radius)
{
	// 壁にあったか調べるために参照
	Stage* pStage = (Stage*)FindObject("Stage");
	if (pStage == nullptr) { return; }
	
	int checkX = (int)(pos.vecX - radius);//左　見たい位置を格納
	int checkZ = (int)(pos.vecZ);//見たい位置を格納
	//移動した場所が壁なら
	if (pStage->IsWall(checkX, checkZ))
	{
		//transform_.position_.vecX = prevPos.vecX;//移動する前に戻る
		pos.vecX = (float)checkX + 1.0f + radius;
	}

	checkX = (int)(pos.vecX + radius);//右　見たい位置を格納
	checkZ = (int)(pos.vecZ);//見たい位置を格納
	//移動した場所が壁なら
	if (pStage->IsWall(checkX, checkZ))
	{
		pos.vecX = (float)checkX - radius;	//半径分戻す
	}
	//移動した場所が壁なら

	checkX = (int)(pos.vecX);//手前 見たい位置を格納
	checkZ = (int)(pos.vecZ - radius);//見たい位置を格納
	if (pStage->IsWall(checkX, checkZ))
	{
		pos.vecZ = (float)checkZ + 1.0f + radius;//戻す
	}

	checkX = (int)(pos.vecX);//見たい位置を格納
	checkZ = (int)(pos.vecZ + radius);//奥　見たい位置を格納
	//移動した場所が壁なら
	if (pStage->IsWall(checkX, checkZ))
	{
		pos.vecZ = (float)checkZ - radius;//-モデルの半径分して戻す
	}
};

void Player::Draw()
{
	//モデルの描画
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Player::Release()
{
}

//衝突したときに呼ばれる
void Player::OnCollision(GameObject * pTarget)
{
	if (pTarget->GetObjectName() == "PlainFloor")
	{
		moveVec_ = XMVectorSet(0, 0, 0, 0);
	}
}