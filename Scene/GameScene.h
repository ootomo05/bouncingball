#pragma once
#include "../Engine/GameObject.h"

//テストシーンを管理するクラス
class GameScene : public GameObject
{
	int currentTime_;
	const int LIMIT_TIME = 60 * 60;
	int hNumsIMage_[10];
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	GameScene(GameObject* parent);

	//初期化
	//初期化
	void Initialize() override;

	//更新
	//引数 なし
	void Update() override;

	//描画
	//引数 なし
	void Draw() override;

	//開放
	//引数 なし
	void Release() override;
};