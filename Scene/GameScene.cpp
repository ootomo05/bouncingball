#include "GameScene.h"
#include "../Player.h"
#include "../Score.h"
#include "../Stage.h"
#include "../DestWall.h"
#include "../NPCPlayer.h"
#include "../Engine/SceneManager.h"
#include "../Engine/Image.h"

GameScene::GameScene(GameObject * parent)
	: GameObject(parent, "GameScene"), currentTime_(0), hNumsIMage_{ -1 }
{
}

//初期化
void GameScene::Initialize()
{
	Instantiate<Stage>(this);  //ステージの生成
	Instantiate<Player>(this); //プレイヤーの生成
	Instantiate<NPCPlayer>(this); //NPCプレイヤーの生成

	//各ファイルを読み込む
	std::string fileName[10] = {
		"Assets\\Nums\\Num_Image_0.png",
		"Assets\\Nums\\Num_Image_1.png",
		"Assets\\Nums\\Num_Image_2.png",
		"Assets\\Nums\\Num_Image_3.png",
		"Assets\\Nums\\Num_Image_4.png",
		"Assets\\Nums\\Num_Image_5.png",
		"Assets\\Nums\\Num_Image_6.png",
		"Assets\\Nums\\Num_Image_7.png",
		"Assets\\Nums\\Num_Image_8.png",
		"Assets\\Nums\\Num_Image_9.png",
	};

	//数値イメージをロード
	for (int i = 0; i < 10; i++)
	{
		hNumsIMage_[i] = Image::Lode(fileName[i]);
		assert(hNumsIMage_[i] >= 0);
	}
}

//更新
void GameScene::Update()
{
	// 制限時間になったらリザルトシーンへ
	if (currentTime_ > LIMIT_TIME)
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_RESULT);
	}

	currentTime_++; //時間を進める
}

//描画
void GameScene::Draw()
{
	//描画位置の指定
	Transform trans;
	trans.position_.vecY = 500.0f / 600.0f;
	trans.position_.vecX = -100.0f / 800.0f;
	trans.Calclation();

	std::string numStr = std::to_string(currentTime_);
	for (int i = 0; i < numStr.size(); i++)
	{
		//描画位置の指定
		trans.position_.vecX += 64.0f / 800.0f;
		trans.Calclation();
		
		//描画
		Image::SetTransform(hNumsIMage_[numStr[i] - '0'], trans);
		Image::Draw(hNumsIMage_[numStr[i] - '0']);
	}
}

//開放
void GameScene::Release()
{
}
