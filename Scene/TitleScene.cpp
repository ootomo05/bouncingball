#include "TitleScene.h"
#include "../Engine/Image.h"
#include "../Engine/Input.h"
#include "../Engine/SceneManager.h"
#include <string>
#include "../ModeSelect.h"
#include "../GameMode.h"
#include "../GameModeFightNPC.h"
#include "../GameModeFightPlayer.h"

//タイトルシーン
TitleScene::TitleScene(GameObject * parent) : 
	GameObject(parent,"TitleScene"),hTitleImage_(-1), hModeImage_(-1), hNumsIMage_{-1}
{
	
	for (int i = 0; i < GAME_MODE_MAX; i++)pGameMode_[i] = nullptr;
}

//初期化
void TitleScene::Initialize()
{
	//画像をロード
	hTitleImage_ = Image::Lode("Assets\\TitleScene.png");
	assert(hTitleImage_ >= 0);
	
	//ゲームモードの登録
	pGameMode_[GAME_MODE_1P] = new GameModeFightNPC(this);
	assert(pGameMode_[GAME_MODE_1P] != nullptr);

	pGameMode_[GAME_MODE_2P] = new GameModeFightPlayer(this);
	assert(pGameMode_[GAME_MODE_2P] != nullptr);
}

//更新
void TitleScene::Update()
{
	if (Input::IsKey(DIK_SPACE))
	{
		pGameMode_[GAME_MODE_1P]->StartGame();
	}
}

//描画
void TitleScene::Draw()
{
	//画像の描画
	Image::SetTransform(hTitleImage_, transform_);
	Image::Draw(hTitleImage_);
}

//解放
void TitleScene::Release()
{
}
