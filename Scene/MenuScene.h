#pragma once
#include "../Engine/GameObject.h"

//テストシーンを管理するクラス
class MenuScene : public GameObject
{
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	MenuScene(GameObject* parent);

	//初期化
	//初期化
	void Initialize() override;

	//更新
	//引数 なし
	void Update() override;

	//描画
	//引数 なし
	void Draw() override;

	//開放
	//引数 なし
	void Release() override;
};