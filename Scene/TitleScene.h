#pragma once
#include "../Engine/GameObject.h"
#include "../GameMode.h"

//テストシーンを管理するクラス
class TitleScene : public GameObject
{
	enum GAME_MODE
	{
		GAME_MODE_1P = 0, //NPCと対戦
		GAME_MODE_2P,     //2人対戦
		GAME_MODE_MAX
	};

	int hTitleImage_; //ゲームタイトル画像
	int hNumsIMage_[10]; //数字
	int hModeImage_; //モード選択
	GameMode* pGameMode_[GAME_MODE_MAX]; //ゲームモード

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	TitleScene(GameObject* parent);

	//初期化
	//引数 なし
	void Initialize() override;

	//更新
	//引数 なし
	void Update() override;

	//描画
	//引数 なし
	void Draw() override;

	//開放
	//引数 なし
	void Release() override;
};