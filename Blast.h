#pragma once
#include "Engine/GameObject.h"

//爆風クラス
class Blast : public GameObject
{
	struct Vec2
	{
		float x;
		float y;
	};

	Vec2 baseVec;
	int hModel_; //モデル番号
	const float SPEED = 0.4f; //速さ
	const float KILL_LENGE = 5.0f; //この距離移動したら死ぬ
	XMVECTOR moveVec_; //移動ベクトル

	//移動関数
	//引数 なし
	void Move();
	void OnCollision(GameObject * pTarget);
public:
	Blast(GameObject* pParent);
	~Blast();

	//初期化
	//引数 なし
	virtual void Initialize() override;

	//更新
	//引数 なし
	virtual void Update() override;

	//描画
	//引数 なし
	virtual void Draw() override;

	//開放
	//引数 なし
	virtual void Release() override;

	//移動方向を決める
	//引数 moveVec 移動方向
	void SetMoveVec(XMVECTOR moveVec) { moveVec_ = moveVec; }

	void SetBasePos(const float x, const float y) { baseVec.x = x; baseVec.y = y;};
};

