#pragma once
#include "Engine/Vec2.h"
#include <vector>


//AStar用のNodeクラス
class AStarNode
{
	float cost_;      //実コスト
	float minCost_;   //最小コスト 
	float direction_; //推定コスト

	Vec2 pos_; //座標

	bool isOpen_; //開いているか
	bool isClose_; //閉じているか

	AStarNode* pPrevNode_; //親ノード


public:

	//コンストラクタ
	//引数 cost       実コスト
	//引数 direction  推定コスト
	//引数 pos        ノード座標 
	AStarNode(float cost, float direction, const Vec2& pos);

	//デストラクタ
	~AStarNode();

	//~~~~~~~~~~~ゲッター~~~~~~~~~~~~~

	//コストの取得
	//引数 なし
	//戻値 コスト
	float GetCost() { return cost_; };

	//最小コスト
	//引数 なし
	//戻値 最小コスト
	float GetMinCost() { return minCost_; };

	//ゴールまでの距離(推定コスト)を取得
	//引数 なし
	//戻値 ゴールまでの距離(推定コスト)
	float GetDirection() { return direction_; };

	//ノード座標を取得
	//引数 なし
	//戻値 ノード座標
	Vec2 GetPos() { return pos_; };

	//親ノードを取得
	//引数 なし
	//戻値 親ノードポインタ
	AStarNode* GetPrevNode() { return pPrevNode_; };

	//ノードが開いているか判定
	//引数 なし
	//開いていたら trueを返す
	bool IsOpen() { return isOpen_; };

	//ノードが閉じているか判定
	//引数 なし
	//閉じていたら trueを返す
	bool IsClose() { return isClose_; };


	//~~~~~~~~~~~~~セッター~~~~~~~~~~~~~~~~

	//親ノードをセット
	//引数 AStarNode* ノードポインタ
	void SetPrevNode(AStarNode* prevNode) { pPrevNode_ = prevNode; };

	//最小コストを更新
	//引数 minCost 最小コスト
	void SetMinCost(float minCost) { minCost_ = minCost; };

	//isOpen_フラグを更新
	//引数 isOpen
	void SetOpen(bool isOpen) { isOpen_ = isOpen; };

	//isClose_フラグを更新
	//引数 isClose
	void SetClose(bool isClose) { isClose_ = isClose; };

	//実コストを更新
	//引数 cost 実コスト
	void SetCost(float cost) { cost_ = cost; };
};

