#pragma once
#include "GameMode.h"

//プレイヤーと対戦モードクラス
class GameModeFightPlayer : public GameMode
{
public:
	GameModeFightPlayer(GameObject* parent);
	~GameModeFightPlayer();
	//ゲームスタート
	void StartGame();

};

