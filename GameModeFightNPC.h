#pragma once
#include "GameMode.h"

//NPC対戦モードクラス
class GameModeFightNPC : public GameMode
{
public:
	// コンストラクタ
	// 引数 parent 親 
	GameModeFightNPC(GameObject* parent);

	// デストラクタ
	~GameModeFightNPC();

	// ゲーム開始
	// 引数 なし
	void StartGame();
};