#include "AStar.h"
#include "AStarNode.h"
#include "Engine/Global.h"
#include <assert.h>
#include "Stage.h"

bool Sort(AStarNode* node1, AStarNode* node2);

AStar::AStar(uint32_t width, uint32_t height) : 
	MAP_HEIGHT(height),MAP_WIDTH(width), pppNodes_(nullptr), IsRunAStar_(true),goalPos_(0,0)
{
	pNodeOpenList_.clear(); //オープンリスト
	pppNodes_ = new AStarNode**[MAP_WIDTH];
	for (uint32_t i = 0; i < MAP_WIDTH; i++)
	{
		pppNodes_[i] = new AStarNode*[MAP_HEIGHT];
	}

	shortestPath_.clear();
}

AStar::~AStar()
{
	//解放処理
	for (uint32_t i = 0; i < MAP_WIDTH; i++)
	{
		for (uint32_t j = 0; j < MAP_HEIGHT; j++)
		{
			SAFE_DELETE(pppNodes_[i][j]);
		}
	}
	for (uint32_t i = 0; i < MAP_WIDTH; i++)
	{
		SAFE_DELETE_ARRAY(pppNodes_[i]);
	}
	SAFE_DELETE_ARRAY(pppNodes_);
}

void AStar::Init(const Vec2 & goalPos, Stage*  stage)
{
	//チェック
	assert(stage != nullptr);

	//クリア
	pNodeOpenList_.clear();
	shortestPath_.clear();

	//ノードの初期化
	for (uint32_t i = 0; i < MAP_WIDTH; i++)
	{
		for (uint32_t j = 0; j < MAP_HEIGHT; j++)
		{
			//推定距離の計算
			Vec2 nodePos(i, j);
			nodePos - goalPos;
			float length = nodePos.Length().x / float(MAP_WIDTH * MAP_HEIGHT);

			//コストの設定
			float score = stage->GetCost(CELL_TYPE(stage->GetMapData(i, j)));

			//ノード作成
			pppNodes_[i][j] = new AStarNode(score,length,Vec2(i,j));
		}
	}
	
	//ゴールノードの初期化
	pppNodes_[static_cast<uint32_t>(goalPos.x)][static_cast<uint32_t>(goalPos.y)]->SetCost(0.0f);
	goalPos_ = goalPos;
}

void AStar::Algorithm(const Vec2 & startPos)
{
	//探査ノードのリスト
	std::list<AStarNode*> openList;
	openList.clear();

	//スタート
	AStarNode* pStart = pppNodes_[static_cast<uint32_t>(startPos.x)][static_cast<uint32_t>(startPos.y)];
	pStart->SetMinCost(0);
	pStart->SetMinCost(pStart->GetDirection());

	//スタートとゴールが同一地点なら処理はしない
	if (startPos.GetUIntX() == goalPos_.GetUIntX() && startPos.GetUIntY() == goalPos_.GetUIntY())
	{
		return;
	}

	openList.push_back(pStart); //最初に探査するノードを追加

	//リストが空になるまで繰り替えす
	while (openList.empty() != true)
	{
		//起点ノードをリストの先頭から取り出す
		AStarNode* pBasicPoint = openList.front(); //起点ノード
		pBasicPoint->SetOpen(false);
		openList.pop_front();

		//ゴールにたどり着いたら終了
		if (pBasicPoint->GetPos().GetUIntX() == goalPos_.GetUIntX() && pBasicPoint->GetPos().GetUIntY() == goalPos_.GetUIntY())
		{
			break;
		}

		pBasicPoint->SetClose(true); //探査済みにする

		//4方向に隣接するノードを調べる
		for (uint32_t i = 0; i < 4; i++)
		{
			//上下左右に隣接するノードを取得
			unsigned int searchPosX = pBasicPoint->GetPos().GetUIntX() + direcions[i].GetUIntX();
			unsigned int searchPosY = pBasicPoint->GetPos().GetUIntY() + direcions[i].GetUIntY();

			int test = pBasicPoint->GetPos().GetUIntY();
			AStarNode* pSearchNode = pppNodes_[searchPosX][searchPosY];

			//壁は調べない
			if (pSearchNode->GetCost() < 0)
			{
				continue;
			}

			//コストの計算
			float cost = pBasicPoint->GetMinCost() - pBasicPoint->GetDirection() + pSearchNode->GetCost() + pSearchNode->GetDirection();

			//オープンリストに追加されてる場合
			if (pSearchNode->IsClose() || pSearchNode->IsOpen())
			{
				//最小コストに更新してく
				if (cost < pSearchNode->GetMinCost())
				{
					pSearchNode->SetMinCost(cost);
					pSearchNode->SetPrevNode(pBasicPoint);
				}
			}
			//両方に追加されてない場合
			else
			{
				//最小コストを更新
				//オープンリストに追加
				pSearchNode->SetMinCost(cost);
				pSearchNode->SetPrevNode(pBasicPoint);
				pSearchNode->SetOpen(true);

				openList.push_back(pSearchNode);
			}
		}
		openList.sort(&Sort); //最小コストが一番小さいノードが先頭に来るようにする
	}

	ShortPath(pppNodes_[goalPos_.GetUIntX()][goalPos_.GetUIntY()]); //最短経路をリストに追加
}

//最短経路をリストに追加していく
void AStar::ShortPath(AStarNode * pNode)
{
	if (pNode != nullptr)
	{
		ShortPath(pNode->GetPrevNode());
		shortestPath_.push_back(pNode->GetPos());
	}
	return;
}

//ゴールまでたどり着く最短の座標を渡す
Vec2 AStar::GetNextPoint()
{
	//
	if (shortestPath_.empty())return Vec2(-1.0f,-1.0f);

	//リストの前方から出していく
	Vec2 nextPoint = *shortestPath_.begin();
	shortestPath_.pop_front();

	return nextPoint;
};

bool Sort(AStarNode* node1, AStarNode* node2)
{
	return node1->GetMinCost() < node2->GetMinCost();
}