#include "Score.h"
#include "Engine/Image.h"

Score::Score(GameObject * parent) : GameObject(parent,"Score"),hScoreImage_(-1)
{
}

Score::~Score()
{
}

void Score::Initialize()
{
	hScoreImage_ = Image::Lode("Assets\\Score.png");
	assert(hScoreImage_ >= 0);
}

void Score::Update()
{
}

void Score::Draw()
{
	Image::SetTransform(hScoreImage_, transform_);
	Image::Draw(hScoreImage_);
}

void Score::Release()
{
}
