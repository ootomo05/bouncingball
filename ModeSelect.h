#pragma once
#include "Engine/GameObject.h"

class ModeSelect :public GameObject
{
	//ゲームモード
	enum GAME_MODE
	{
		VS_NPC = 0, //NPC対戦
		VS_PLAYER,   //2人対戦
		GAME_MODE_MAX
	};

	int hModeName_[GAME_MODE_MAX];
	int currentGameMode_; //選択されているモード
public:

	ModeSelect(GameObject* pParent);
	~ModeSelect();
	// GameObject を介して継承されました

	//初期化
	//引数 なし
	virtual void Initialize() override;

	//更新
	//引数 なし
	virtual void Update() override;

	//描画
	//引数 なし
	virtual void Draw() override;

	//解放
	//引数 なし
	virtual void Release() override;
};

