//───────────────────────────────────────
 // テクスチャ＆サンプラーデータのグローバル変数定義
//───────────────────────────────────────
Texture2D	g_texture : register(t0);	//テクスチャー
SamplerState	g_sampler : register(s0);	//サンプラー

//───────────────────────────────────────
 // コンスタントバッファ
// DirectX 側から送信されてくる、ポリゴン頂点以外の諸情報の定義
//───────────────────────────────────────
cbuffer global
{
	float4x4	matWVP;			// ワールド・ビュー・プロジェクションの合成行列
	float4x4	matW;	        //ワールド行列
	float4		diffuseColor;	// ディフューズカラー（マテリアルの色）
	bool		isTexture;		// テクスチャ貼ってあるかどうか
};

//───────────────────────────────────────
// 頂点シェーダー出力＆ピクセルシェーダー入力データ構造体
//───────────────────────────────────────
struct VS_OUT
{
	float4 pos      : SV_POSITION;	//位置
	float2 uv	    : TEXCOORD;     //UV座標
	float4 color	: COLOR;	    //色（明るさ）
};

//───────────────────────────────────────
// 頂点シェーダ
//───────────────────────────────────────

//頂点の数呼ばれる
//引数	pos  	頂点情報
//引数	uv      uv情報
//引数   normal  法線情報
VS_OUT VS(float4 pos : POSITION, float4 uv : TEXCOORD, float4 normal : NORMAL)
{
	
	//ピクセルシェーダーへ渡す情報
	VS_OUT outData;

	//ローカル座標に、ワールド・ビュー・プロジェクション行列をかけて
	//スクリーン座標に変換し、ピクセルシェーダーへ
	outData.pos = mul(pos, matWVP);
	outData.uv = uv;

	//法線を回転させる
	normal = mul(normal, matW);

	//陰の計算
	float4 light = float4(-1, 0.5, -
		0.7, 0);//光源の設定
	light = normalize(light);//単位ベクトルにする
	//後々楽になる
	//法線と光源の内積を計算して
	//clampで内積の結果を0~1の範囲にしている
	outData.color = clamp(dot(normal, light),0,1);

	//まとめて出力
	return outData;
}

//───────────────────────────────────────
// ピクセルシェーダ
//───────────────────────────────────────
//ピクセルの数分呼ばれる
float4 PS(VS_OUT inData) : SV_Target
{ 
	//テクスチャが貼られていても、はられていなくても
	//両方で使うので用意しとく
	//増やす色の値RGB
	//値を0.0~1.0に設定すると対応した色が増える
	float4 ambientValu = float4(0.2, 0.2, 0.2, 1);
	float4 diffuse = float4(0,0,0,0);	//物体に反射した色
	float4 ambient = float4(0,0,0,0);	//環境からの色

	if (isTexture == true)
	{
		//テクスチャが張られているとき
		//自然な感じに描画するためにいろいろする
		diffuse = g_texture.Sample(g_sampler, inData.uv) * inData.color; //拡散反射光
		ambient = g_texture.Sample(g_sampler, inData.uv) * ambientValu;  //環境光を加える
	}
	else
	{
		//テクスチャが張られてないとき
		//マテリアルに設定されている色で描画
		diffuse = diffuseColor * inData.color;
		ambient = diffuseColor * float4(0.2, 0.2, 0.2, 1);
	}

	//最後にピクセルごとに計算した色を付ける
	return diffuse + ambient;
}


