#pragma once
#include "Engine/GameObject.h"

//破壊可能な壁
class DestWall : public GameObject
{
	int hModel_;
public:

	//コンストラクタ
	//引数 pParent
	DestWall(GameObject* pParent);

	//デストラクタ
	~DestWall();

	//初期化
	//引数 なし
	virtual void Initialize() override;

	//更新
	//引数 なし
	virtual void Update() override;

	//描画
	//引数 なし
	virtual void Draw() override;

	//解放
	//引数 なし
	virtual void Release() override;
};

