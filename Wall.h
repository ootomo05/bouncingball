#pragma once
#include "Engine/GameObject.h"

//壁管理クラス
class Wall : public GameObject
{
	int hModel_; //モデル番号
public:

	//コンストラクタ
	//引数 parent 親
	Wall(GameObject* parent);

	//デストラクタ
	~Wall();

	// GameObject を介して継承されました
	//初期化
	virtual void Initialize() override;

	//更新
	virtual void Update() override;

	//描画
	virtual void Draw() override;

	//解放
	virtual void Release() override;
};

