#pragma once
#include "Engine/GameObject.h"
#include <list>
#include "Engine/Vec2.h"

//前方宣言
class AStar;

//NPCプレイヤーを管理するクラス
class NPCPlayer : public GameObject
{
	//方向ベクトル
	Vec2 directions_[4] =
	{
		{1.0f,0.0f},
		{-1.0f,0.0f},
		{0.0f,1.0f},
		{0.0f,-1.0f}
	};

	int hModle_; //モデル番号
	unsigned int timeCount_;  //時間を計測
	const float SPEED = 0.3f; //移動スピード
	XMVECTOR moveVec_;        //移動方向

	AStar* pAstar_; //エースタークラス

	bool isCanDecideRoute_; //ルートを決められる
	bool isUpdateNextPos_;//目標座標を更新
public:

	//コンストラクタ
	//引数 pParent 親
	NPCPlayer(GameObject* pParent);

	//デストラクタ
	~NPCPlayer();

	//初期化
	//引数 なし
	virtual void Initialize() override;

	//更新
	//引数 なし
	virtual void Update() override;

	//移動
	//引数 なし
	void Move();

	//ルートを決める
	//引数 なし
	void DecideRoute();

	//次に移動する座標
	//引数 なし
	//戻値 移動目標
	XMVECTOR NextRoutePos();

	//float型のクランプ
	//引数 val
	//引数 min
	//引数 max
	//戻値
	float FClamp(const float val, const float min, const float max);

	//描画
	//引数 なし
	virtual void Draw() override;

	//解放
	//引数 なし
	virtual void Release() override;
};