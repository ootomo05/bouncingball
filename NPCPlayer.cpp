#include "NPCPlayer.h"
#include "Engine/Model.h"
#include "Stage.h"
#include "Engine/Input.h"
#include "Bomb.h"
#include "Player.h"
#include "AStar.h"
#include "Engine/Input.h"

//コンストラクタ
NPCPlayer::NPCPlayer(GameObject * pParent) 
	: GameObject(pParent,"NPCPlayer"), hModle_(-1), pAstar_(nullptr),
	isCanDecideRoute_(true), isUpdateNextPos_(true)
{
	srand((unsigned int)time(nullptr));
}

//デストラクタ
NPCPlayer::~NPCPlayer()
{
}

//初期化
void NPCPlayer::Initialize()
{
	//モデルのロード
	hModle_ = Model::Load("Assets\\Player.fbx");
	assert(hModle_ >= 0);

	//初期位置
	transform_.position_.vecX = 7.5f;
	transform_.position_.vecZ = 1.5f;
}

//更新
void NPCPlayer::Update()
{
	Move();
	
	//床の色を変更する
	Stage* pStage = (Stage*)FindObject("Stage");
	assert(pStage != nullptr);
	
	//座標をunsigned int型にキャスト
	const unsigned int curentPosX = (unsigned int)transform_.position_.vecX;
	const unsigned int curentPosZ = (unsigned int)transform_.position_.vecZ;

	//座標の床の色を変更
	pStage->SetColorFloor(curentPosX, curentPosZ, CELL_TYPE_GREEN_FLOOR);
}

//移動関数
void NPCPlayer::Move()
{
	//ルートを決める
	DecideRoute();
	
	//次に向かう座標に向かって移動
	XMVECTOR nextPoint = NextRoutePos();
	XMVECTOR curentPos = transform_.position_;

	//目的地についた
	if (curentPos.vecX - 0.5f  < nextPoint.vecX && curentPos.vecX + 0.5f > nextPoint.vecX &&
		curentPos.vecZ - 0.5f < nextPoint.vecZ && curentPos.vecZ + 0.5f > nextPoint.vecZ)
	{
		isUpdateNextPos_ = true;
	}

	//ゴールについた
	if (nextPoint.vecX < 0)
	{
		if (pAstar_ != nullptr)
		{
			nextPoint = curentPos;
			isCanDecideRoute_ = true;
			isUpdateNextPos_ = true;
			SAFE_DELETE(pAstar_);
		}
	}
	
	//移動方向を決める
	moveVec_ = nextPoint - curentPos;
	moveVec_ = XMVector3Normalize(moveVec_);

	//移動
	transform_.position_ += moveVec_ * SPEED;

	//ステージを参照
	Stage* pStage = (Stage*)FindObject("Stage");
	assert(pStage != nullptr);


	//範囲外に出ないようにする
	transform_.position_.vecX = FClamp(transform_.position_.vecX, 0, pStage->GetMapWidht());
	transform_.position_.vecZ = FClamp(transform_.position_.vecZ, 0, pStage->GetMapHeight());
}

void NPCPlayer::DecideRoute()
{
	Stage* pStage = (Stage*)FindObject("Stage");
	assert(pStage != nullptr);

	if (isCanDecideRoute_)
	{
		//エースターアルゴリズムを使用する
		pAstar_ = new AStar(pStage->GetMapWidht(), pStage->GetMapHeight());

		//ゴールをランダムで決める
		int x = 0; //ゴールx座標
		int z = 0; //ゴールz座標

		//通れない壁をゴールに選ばないようにする処理
		while (true)
		{
			x = rand() % (pStage->GetMapWidht() - 2) + 1;
			z = rand() % (pStage->GetMapHeight() - 2) + 1;

			//ゴールの場所がブロックでないなら
			//その場所をゴールにする
			if (pStage->GetCost(CELL_TYPE(pStage->GetMapData(x, z))) > pStage->GetCost(CELL_TYPE_WALL))
			{
				break;
			}
		}

		//ゴール座標とスタート座標を決める
		//スタート座標は自分の現在位置
		Vec2 goalPos = Vec2(x, z);
		Vec2 startPos = Vec2(transform_.position_.vecX, transform_.position_.vecZ);

		//値が0にならないようにする
		//0だと配列の外を参照してしまうので
		if (goalPos.x < 1) { goalPos.x = 1; }
		if (goalPos.y < 1) { goalPos.y = 1; }
		if (startPos.x < 1)startPos.x = 1;
		if (startPos.y < 1)startPos.y = 1;

		//エースターを実行
		pAstar_->Init(goalPos, pStage);
		pAstar_->Algorithm(startPos);
		isCanDecideRoute_ = false;
	}
};
XMVECTOR NPCPlayer::NextRoutePos()
{
	//次にすすむ座標の位置を更新
	static Vec2 nextPoint = Vec2(0, 0);
	
	if (isUpdateNextPos_)
	{
		if (pAstar_ != nullptr)
		{
			nextPoint = pAstar_->GetNextPoint();

			//キャラクタが道の中心をとおるようにする
			int x = nextPoint.GetUIntX();
			int z = nextPoint.GetUIntY();
			if (x % 2 == 0)nextPoint.x += 1;
			if (z % 2 == 0)nextPoint.y += 1;
			nextPoint.x += 0.5f;
			nextPoint.y += 0.5f;

			isUpdateNextPos_ = false;
		}
	}

	return XMVectorSet(nextPoint.x, 0, nextPoint.y, 0);
}
//float型クランプ
float NPCPlayer::FClamp(const float val, const float min, const float max)
{
	if (val < min)
	{
		return min;
	}
	else if (val > max)
	{
		return max;
	}

	return val;
}

//描画
void NPCPlayer::Draw()
{
	//モデルの描画
	Model::SetTransform(hModle_, transform_);
	Model::Draw(hModle_);
}

//解放
void NPCPlayer::Release()
{
}
