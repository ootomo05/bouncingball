#include "ModeSelect.h"
#include "Engine/Image.h"
#include "Engine/Direct3D.h"
#include <string>

//コンストラクタ
ModeSelect::ModeSelect(GameObject* pParent) : GameObject(pParent,"ModelSelect"),currentGameMode_(VS_NPC)
{
	for (int i = 0; i < GAME_MODE_MAX; i++)
	{
		hModeName_[i] = -1;
	}
}

//デストラクタ
ModeSelect::~ModeSelect()
{

}

//初期化
void ModeSelect::Initialize()
{
	//読み込み準備
	std::string modeImgFile[GAME_MODE_MAX] =
	{
		"Assets\\ModeVsNpc.png",
		"Assets\\ModeVsNpc.png"
	};

	//画像データロード
	for (int i = 0; i < GAME_MODE_MAX; i++)
	{
		hModeName_[i] = Image::Lode(modeImgFile[i]);
		assert(hModeName_[i] >= 0);
	}
}

//更新
void ModeSelect::Update()
{
}

//描画
void ModeSelect::Draw()
{
	//描画位置の指定
	Transform imgDrawTrans;
	imgDrawTrans.position_.vecY = -100.0f / 600.0f;
	imgDrawTrans.Calclation();

	//画像分描画する
	for (int i = 0; i < GAME_MODE_MAX; i++)
	{
		Image::SetTransform(hModeName_[i], imgDrawTrans);
		Image::Draw(hModeName_[i]);

		imgDrawTrans.position_.vecY = -250.0f / 600.0f;
		imgDrawTrans.Calclation();
	}
}

//解放
void ModeSelect::Release()
{
}
