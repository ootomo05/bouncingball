#pragma once
#include "Engine/GameObject.h"

class GameMode 
{
protected:
	GameObject* parent_; //親クラス
public:
	GameMode(GameObject* parent);
	virtual ~GameMode();

	//ゲーム生成
	virtual void StartGame() = 0;
};